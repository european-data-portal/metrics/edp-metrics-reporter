# Metrics Reporter
Provides a .pdf, .ods and .xlsx representation of the metrics measurements. In order to create the .pdf version of the report, a Quickchart Docker image needs to be running. Please see below for further information.

## Table of Contents
- [Metrics Reporter](#metrics-reporter)
  - [Table of Contents](#table-of-contents)
  - [Build](#build)
  - [Run](#run)
  - [Docker](#docker)
  - [Quickchart](#quickchart)
  - [Configuration](#configuration)
    - [Logging](#logging)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ... 
$ mvn package
```
 
## Run

```bash
$ java -jar -Xmx6144m target/metrics-reporter-fat.jar
```

## Docker

Build docker image:
```bash
$ docker build -t edp/metrics-reporter .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 edp/metrics-reporter
```

## Quickchart

To set up Quickchart, pull the Quickchart image from Docker
```bash
$ docker pull ianw/quickchart
```

Build and run it:
```bash
$ docker build -t ianw/quickchart .
$ docker run -p 8085:3400 ianw/quickchart
```

## Configuration

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable                   | Description                                       | Default Value                         |
| :------------------------- | :------------------------------------------------ | :------------------------------------ |
| `PORT`                     | Sets the environment application port             | `8080`                                |
| `QUICKCHART_ADDRESS`       | The address of the Quickchart service             | `quickchart-edp2.okd.fokus.fraunhofer.de:80/chart`                           |
| `METRICS_HOST`             | The host of the Metrics API                       | `europeandataportal.eu`               |
| `METRICS_PORT`             | The port of the Metrics API                       | `80`               |
| `HUB_ADDRESS`              | Full address to which report dataset will be uploaded       | `https://piveau-hub-edp2.okd.fokus.fraunhofer.de/datasets/test?catalogue=edp&data=true`               |
| `LANGUAGES`                | List of languages for which reports are generated | `bg, cs, da, de, el, en, es, et, fi, fr, ga, hr, hu, it, lt, lv, mt, nl, no, pl, pt, ro, sk, sl, sv`               |
| `API_KEY_HUB`              | API key required by Hub for dataset upload        | -               |
| `API_KEY_UPLOAD`           | API key required by data uploader to upload reports | -               |

