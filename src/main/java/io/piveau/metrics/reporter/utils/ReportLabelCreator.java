package io.piveau.metrics.reporter.utils;

import io.piveau.metrics.reporter.model.ReportLabels;
import io.vertx.core.json.JsonObject;

import java.util.Arrays;
import java.util.Locale;

public class ReportLabelCreator {

    private Locale locale;
    private JsonObject translations;
    private JsonObject countries;

    private ReportLabels reportLabels;
    private JsonObject sectionTranslations;

    public ReportLabelCreator(Locale locale, JsonObject translations, JsonObject countries) {
        this.locale = locale;
        this.translations = translations;
        this.countries = countries;

        reportLabels = generateReportLabels();
        sectionTranslations = generateSectionTranslations();
    }

    public JsonObject getCountries() {
        return countries;
    }

    public ReportLabels getReportLabels() {
        return reportLabels;
    }

    public JsonObject getSectionTranslations() {
        return sectionTranslations;
    }

    /*
    * Method to extract report labels from lang-old.json file.
    * The labels are saved into an instance of class ReportLabels.
    */
    private ReportLabels generateReportLabels(){
        JsonObject languageRoot = translations.getJsonObject(locale.getLanguage().toLowerCase());

        if (languageRoot != null) {

            JsonObject msg = languageRoot.getJsonObject("message");

            ReportLabels labels = new ReportLabels();
            labels.setLanguage(locale);

            labels.setDashboardTitle(getKeySave(msg, new String[]{"common", "edp", "title"}));
            labels.setMqaTitle(getKeySave(msg, new String[]{"title"}));
            labels.setMqaIntro(getKeySave(msg, new String[]{"methodology", "intro_text"}));
            labels.setEdpUrl("https://www.europeandataportal.eu");

            labels.setMqaDescription(getKeySave(msg, new String[]{"dashboard", "intro", "desc"}));

            labels.setMetricLabelYes(getKeySave(msg, new String[]{"common", "yes"}));
            labels.setMetricLabelNo(getKeySave(msg, new String[]{"common", "no"}));

            labels.setCatalogueCountry(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "country"}));
            labels.setCatalogueName(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "name"}));
            labels.setAccessibilityAccessUrl(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "accessibility_access_url"}));
            labels.setMachineReadability(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "machine_readability"}));
            labels.setDcatApCompliance(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "dcat_ap"}));
            labels.setKnownLicences(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "known_licences"}));
            labels.setAccessibilityDownloadUrl(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "accessibility_download_url"}));

            labels.setMethodologyHeadline(getKeySave(msg, new String[]{"methodology", "headline"}));
            labels.setMethodologySubHeadline(getKeySave(msg, new String[]{"methodology", "sub_headline"}));
            labels.setMethodologyIntroText(getKeySave(msg, new String[]{"methodology", "intro_text"}));
            labels.setMethodologyQualityHeadline(getKeySave(msg, new String[]{"methodology", "what_is_quality", "headline"}));
            labels.setMethodologyQualityMainText(getKeySave(msg, new String[]{"methodology", "what_is_quality", "main_text"}));
            labels.setMethodologyScopeHeadline(getKeySave(msg, new String[]{"methodology", "scope", "headline"}));
            labels.setMethodologyScopeMainText(getKeySave(msg, new String[]{"methodology", "scope", "main_text"}));
            labels.setMethodologyCoverHeadline(getKeySave(msg, new String[]{"methodology", "cover", "headline"}));
            labels.setMethodologyCoverMainText(getKeySave(msg, new String[]{"methodology", "cover", "main_text"}));
            labels.setMethodologyCoverLinkText(getKeySave(msg, new String[]{"methodology", "cover", "link_text"}));
            labels.setMethodologyProcessHeadline(getKeySave(msg, new String[]{"methodology", "process", "headline"}));
            labels.setMethodologyProcessMainText(getKeySave(msg, new String[]{"methodology", "process", "main_text"}));
            labels.setMethodologyAssumptionsHeadline(getKeySave(msg, new String[]{"methodology", "assumptions", "headline"}));
            labels.setMethodologyAssumptionsMainText(getKeySave(msg, new String[]{"methodology", "assumptions", "main_text"}));
            labels.setMethodologyDimensionsHeadline(getKeySave(msg, new String[]{"dashboard", "dimensions"}));
            labels.setMethodologyDimensionsMainText(getKeySave(msg, new String[]{"methodology", "dimensions", "main_text"}));
            labels.setNoDataError(getKeySave(msg, new String[]{"no_data"}));

            labels.setCatalogueRating(getKeySave(msg, new String[]{"dashboard", "overview", "table_title", "rating"}));
            labels.setNavigationDashboard(getKeySave(msg, new String[]{"navigation", "dashboard"}));
            labels.setNotAvailable(getKeySave(msg, new String[]{"common", "notavailable"}));
            labels.setPageTranslation(getKeySave(msg, new String[]{"common", "page"}));

            labels.setScoreBad(getKeySave(msg, new String[]{"methodology", "scoring", "table", "bad"}));
            labels.setScoreExcellent(getKeySave(msg, new String[]{"methodology", "scoring", "table", "excellent"}));
            labels.setScoreGood(getKeySave(msg, new String[]{"methodology", "scoring", "table", "good"}));
            labels.setScoreJustEnough(getKeySave(msg, new String[]{"methodology", "scoring", "table", "just_enough"}));

            return labels;
        } else {
            return null;
        }
    }

    private JsonObject generateSectionTranslations(){
        JsonObject languageRoot = translations.getJsonObject(locale.getLanguage().toLowerCase());

        if (languageRoot != null) {
            JsonObject msg = languageRoot.getJsonObject("message");

            return new JsonObject()
                    .put("accessibility", getKeySave(msg, new String[]{"dashboard", "accessibility", "title"}))
                    .put("accessUrlStatusCodes", getKeySave(msg, new String[]{"dashboard", "accessibility", "plots", "title", "accessUrlStatusCodes"}))
                    .put("downloadUrlAvailability", getKeySave(msg, new String[]{"dashboard", "accessibility", "plots", "title", "downloadUrlAvailability"}))
                    .put("downloadUrlStatusCodes", getKeySave(msg, new String[]{"dashboard", "accessibility", "plots", "title", "downloadUrlStatusCodes"}))

                    .put("contextuality", getKeySave(msg, new String[]{"dashboard", "contextuality", "title"}))
                    .put("dateIssuedAvailability", getKeySave(msg, new String[]{"dashboard", "contextuality", "plots", "title", "dateIssuedAvailability"}))
                    .put("dateModifiedAvailability", getKeySave(msg, new String[]{"dashboard", "contextuality", "plots", "title", "dateModifiedAvailability"}))
                    .put("byteSizeAvailability", getKeySave(msg, new String[]{"dashboard", "contextuality", "plots", "title", "byteSizeAvailability"}))
                    .put("rightsAvailability", getKeySave(msg, new String[]{"dashboard", "contextuality", "plots", "title", "rightsAvailability"}))

                    .put("reusability", getKeySave(msg, new String[]{"dashboard", "reusability", "title"}))
                    .put("contactPointAvailability", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "contactPointAvailability"}))
                    .put("licenceAvailability", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "licenceAvailability"}))
                    .put("licenceAlignment", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "licenceAlignment"}))
                    .put("accessRightsAvailability", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "accessRightsAvailability"}))
                    .put("publisherAvailability", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "publisherAvailability"}))
                    .put("accessRightsAlignment", getKeySave(msg, new String[]{"dashboard", "reusability", "plots", "title", "accessRightsAlignment"}))

                    .put("interoperability", getKeySave(msg, new String[]{"dashboard", "interoperability", "title"}))
                    .put("formatMediaTypeNonProprietary", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "formatMediaTypeNonProprietary"}))
                    .put("formatMediaTypeAlignment", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "formatMediaTypeAlignment"}))
                    .put("formatMediaTypeMachineReadable", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "formatMediaTypeMachineReadable"}))
                    .put("dcatApCompliance", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "dcatApCompliance"}))
                    .put("mediaTypeAvailability", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "mediaTypeAvailability"}))
                    .put("formatAvailability", getKeySave(msg, new String[]{"dashboard", "interoperability", "plots", "title", "formatAvailability"}))

                    .put("findability", getKeySave(msg, new String[]{"dashboard", "findability", "title"}))
                    .put("temporalAvailability", getKeySave(msg, new String[]{"dashboard", "findability", "plots", "title", "temporalAvailability"}))
                    .put("spatialAvailability", getKeySave(msg, new String[]{"dashboard", "findability", "plots", "title", "spatialAvailability"}))
                    .put("keywordAvailability", getKeySave(msg, new String[]{"dashboard", "findability", "plots", "title", "keywordAvailability"}))
                    .put("categoryAvailability", getKeySave(msg, new String[]{"dashboard", "findability", "plots", "title", "categoryAvailability"}))

                    .put("yes", getKeySave(msg, new String[]{"common", "yes"}))
                    .put("no", getKeySave(msg, new String[]{"common", "no"}))
                    .put("countries", countries);
        } else {
            return null;
        }
    }


    private String getKeySave(JsonObject root, String[] keys) {
        JsonObject lastNode = getKeyRecursively(root, keys);

        if (lastNode != null) {
            String label = lastNode.getString(keys[keys.length - 1]);
            return label != null ? label : "Key not found";
        } else {
            return "Key not found";
        }
    }

    private JsonObject getKeyRecursively(JsonObject root, String[] keys) {
        if (root == null)
            return null;

        if (keys.length == 1)
            return root;
        return getKeyRecursively(root.getJsonObject(keys[0]), Arrays.copyOfRange(keys, 1, keys.length));
    }


}