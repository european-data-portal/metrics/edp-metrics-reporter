package io.piveau.metrics.reporter.utils;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import io.piveau.metrics.reporter.model.Styles;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

public class TableCreator {

    private String[] headerCells;
    private JsonArray valueArray;
    private Table table;
    private Styles styles;
    private JsonObject translations;

    public TableCreator(String[] headerCells, JsonArray valueArray, JsonObject translations, Styles styles) {
        this.headerCells = headerCells;
        this.valueArray = valueArray;
        this.translations = translations;
        this.styles = styles;
    }


    /**
     * Method creates a table with header cells as specified in String[] header Cells.
     * Values for the table are extracted from the JsonArray of the TableCreator object.
     */
    public Table createTable() {
        table = new Table(UnitValue.createPercentArray(headerCells.length)).useAllAvailableWidth();

        for (String cellName : headerCells) {
            addStyledHeaderCell(cellName);
        }

        valueArray.forEach(item -> {
            JsonObject obj = (JsonObject) item;
            JsonObject info = obj.getJsonObject("info");

            // Catalogue country
            addNewCellWithStringValue(info, "spatial", true);

            // Catalogue name
            addNewCellWithStringValue(info, "title", false);

            JsonObject accessibility = obj.getJsonObject("accessibility");
            // Accessibility Access URL
            addNewCellWithPercentageValue(accessibility, "accessUrlStatusCodes");
            // Accessibility Download URL
            addNewCellWithPercentageValue(accessibility, "downloadUrlStatusCodes");

            JsonObject interoperability = obj.getJsonObject("interoperability");
            // DCAT AP compliance
            addNewCellWithPercentageValue(interoperability, "dcatApCompliance");
            // Machine readability
            addNewCellWithPercentageValue(interoperability, "formatMediaTypeMachineReadable");

            // Catalogue Rating
            addScoreValue(obj, "score");

        });

        return table;
    }

    /*
     * Helper Methods for table creation
     */

    private void addNewCellWithStringValue(JsonObject motherObject, String key, boolean isCountry) {
        if (motherObject.getString(key) == null) {
            table.addCell(new Cell().add(new Paragraph("-")).addStyle(styles.getTableBodyStyle()));
        } else {
            if (isCountry) {
                if (this.translations.containsKey(motherObject.getString("spatial"))) {
                    table.addCell(new Cell().add(new Paragraph(this.translations.getString(motherObject.getString("spatial")))).addStyle(styles.getTableBodyStyle()));
                } else {
                    table.addCell(new Cell().add(new Paragraph("-")).addStyle(styles.getTableBodyStyle()));
                }
            } else {
                table.addCell(new Cell().add(new Paragraph(motherObject.getString(key))).addStyle(styles.getTableBodyStyle()));
            }
        }
    }

    private void addNewCellWithPercentageValue(JsonObject motherObject, String key) {
        JsonArray keyArray = motherObject.getJsonArray(key);
        if (keyArray == null || keyArray.isEmpty()) {
            valueNotAvailable();
        } else {
            if (keyArray.getJsonObject(0).getString("name").equals("yes") || keyArray.getJsonObject(0).getString("name").equals("no")) {
                keyArray.forEach(item -> {
                    JsonObject itemObject = (JsonObject) item;
                    if (itemObject.getString("name").equals("yes")) {
                        int percentage = (int) Math
                                .floor(itemObject.getDouble("percentage"));

                        table.addCell(new Cell().add(new Paragraph(percentage + "%")).addStyle(styles.getTableBodyStyle()));
                    }
                });
            } else {
                ArrayList<Boolean> contains200 = new ArrayList<>();
                keyArray.forEach(item -> {
                    JsonObject itemObject = (JsonObject) item;
                    if (itemObject.getString("name").equals("200")) {
                        int percentage = (int) Math
                                .floor(itemObject.getDouble("percentage"));

                        table.addCell(new Cell().add(new Paragraph(percentage + "%")).addStyle(styles.getTableBodyStyle()));
                        contains200.add(true);
                    }
                });
                if (contains200.isEmpty()) {
                    table.addCell(new Cell().add(new Paragraph("0%")).addStyle(styles.getTableBodyStyle()));
                }
            }

        }
    }

    private void addScoreValue(JsonObject motherObject, String key) {
        if (motherObject.getInteger(key) == null) {
            table.addCell(new Cell().add(new Paragraph("-")).addStyle(styles.getTableBodyStyle()));
        } else {
//            Excellent: 351 - 405
//            Good: 221 - 350
//            Just enough: 121 - 220
//            Bad: 0 - 120
            int score = motherObject.getInteger(key);
            if (score < 121) {
                table.addCell(new Cell().add(new Paragraph(translations.getJsonObject("scoreTranslations").getString("bad")).addStyle(styles.getTableBodyStyle())));
            } else if (score < 221) {
                table.addCell(new Cell().add(new Paragraph(translations.getJsonObject("scoreTranslations").getString("justEnough")).addStyle(styles.getTableBodyStyle())));
            } else if (score < 351) {
                table.addCell(new Cell().add(new Paragraph(translations.getJsonObject("scoreTranslations").getString("good")).addStyle(styles.getTableBodyStyle())));
            } else if (score < 405) {
                table.addCell(new Cell().add(new Paragraph(translations.getJsonObject("scoreTranslations").getString("excellent")).addStyle(styles.getTableBodyStyle())));
            }
        }
    }

    private void addStyledHeaderCell(String cellName) {
        table.addHeaderCell(new Cell().add(new Paragraph(cellName)).addStyle(styles.getTableHeaderStyle()));
    }

    private void valueNotAvailable() {
        table.addCell(new Cell().add(new Paragraph("n/a")).addStyle(styles.getTableBodyStyle()));
    }
}