package io.piveau.metrics.reporter.utils;

import com.itextpdf.layout.Style;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import io.piveau.metrics.reporter.model.Catalogue;
import io.piveau.metrics.reporter.model.Section;
import io.piveau.metrics.reporter.model.Styles;
import io.piveau.metrics.reporter.model.charts.BarChartBundle;
import io.piveau.metrics.reporter.model.charts.ChartBundle;
import io.piveau.metrics.reporter.model.charts.DoughnutChartBundle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PdfUtils {

    /* Method to generate iText paragraphs and set the font */
    public static Paragraph generateParagraph(String content, Style style) {
        return new Paragraph(content).addStyle(style);
    }

    public static Cell getCellWithCenteredText(String content, Style style){
        return new Cell()
                .add(PdfUtils.generateParagraph(content, style))
                .setTextAlignment(TextAlignment.CENTER)
                .setBorder(Border.NO_BORDER);
    }

    public static Cell saveChartBundleHeadingAndImageToCell(ChartBundle chartBundle, Styles styles) {
        Cell chartHeading = new Cell()
                .add(PdfUtils.generateParagraph(chartBundle.getChartName(), styles.getBoldParagraphStyle()))
                .setTextAlignment(TextAlignment.CENTER)
                .setBorder(Border.NO_BORDER);

        Cell chartImage = new Cell();

        if (!chartBundle.getHasError()) {
            chartImage.add(chartBundle.getImage().setAutoScale(true));
        } else {
            chartImage.add(PdfUtils.generateParagraph(chartBundle.getErrorMessage(), styles.getParagraphStyle()))
                    .setTextAlignment(TextAlignment.CENTER);
        }

        chartImage.setBorder(Border.NO_BORDER);

        Table nestedTable = new Table(1)
                .useAllAvailableWidth()
                .addCell(chartHeading)
                .addCell(chartImage);

        return new Cell().setBorder(Border.NO_BORDER).add(nestedTable);
    }

    public static void extractSectionsFromJson(Catalogue catalogue, ReportLabelCreator reportLabelCreator, JsonObject doughnut, JsonObject bar) {
        List<Section> sections = new ArrayList<>();
        List<String> sectionNames = List.of("contextuality",
                "accessibility",
                "reusability",
                "interoperability",
                "findability");

        JsonObject sectionTranslations = reportLabelCreator.getSectionTranslations();

        for (String sectionName : sectionNames) {
            Section section = new Section(catalogue.getMetrics().getJsonObject(sectionName), doughnut, bar,
                    sectionTranslations.getString(sectionName), reportLabelCreator.getReportLabels().getNoDataError());

            PdfUtils.generateChartBundles(section, sectionTranslations);
            sections.add(section);
        }

        catalogue.setSections(sections);
    }

    public static void generateChartBundles(Section section, JsonObject sectionTranslations) {

        JsonObject chartTranslations = new JsonObject()
                .put("yes", sectionTranslations.getValue("yes"))
                .put("no", sectionTranslations.getValue("no"));

        for (Map.Entry<String, Object> entry : section.getSectionData()) {
            JsonArray chartData = (JsonArray) entry.getValue();

            if (chartData != null && !chartData.isEmpty()) {
                JsonObject obj = chartData.getJsonObject(0);
                if (obj.getString("name").equals("yes") || obj.getString("name").equals("no")) {
                    section.addChartBundle(new DoughnutChartBundle(sectionTranslations.getString(entry.getKey()), chartData, section.getDoughnut(), chartTranslations, false, section.getChartErrorMessage()));
                } else {
                    section.addChartBundle(new BarChartBundle(sectionTranslations.getString(entry.getKey()), chartData, section.getBar(), false, section.getChartErrorMessage()));
                }
            } else {
                section.addChartBundle(new DoughnutChartBundle(sectionTranslations.getString(entry.getKey()), null, null, chartTranslations, true, section.getChartErrorMessage()));
            }
        }
    }

}