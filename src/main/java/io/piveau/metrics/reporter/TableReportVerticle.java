package io.piveau.metrics.reporter;

import io.piveau.metrics.reporter.model.Catalogue;
import io.piveau.metrics.reporter.model.ReportTask;
import io.piveau.metrics.reporter.utils.PdfUtils;
import io.piveau.metrics.reporter.utils.ReportLabelCreator;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.json.JsonObject;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class TableReportVerticle extends AbstractVerticle {

    static final String ADDRESS = "xlsx-ods-message-receiver";

    private static final Logger log = LoggerFactory.getLogger(TableReportVerticle.class);

    private JsonObject translations;
    private JsonObject countries;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, message -> {
            // regular JSON decode() / mapTo() doesn't work
            JsonObject received = new JsonObject(message.body().toString());
            ReportTask task = new ReportTask(
                    new Locale(received.getString("languageCode")),
                    received.getString("reportDir"),
                    received.getJsonArray("globalDashboard"),
                    received.getJsonArray("catalogues").stream()
                            .map(entry -> (JsonObject) entry)
                            .collect(Collectors.toList())
            );

            String reportFileXlsx = task.getPath() + ".xlsx";
            String reportTmpFileXlsx = reportFileXlsx + ".tmp";

            createXlsx(task, reportFileXlsx, reportTmpFileXlsx).setHandler(createXlsx -> {
                if (createXlsx.succeeded()) {
                    log.info("Created XLSX report for language [{}]", task.getLanguageCode());

                    String reportFileOds = task.getPath() + ".ods";
                    String reportTmpFileOds = reportFileOds + ".tmp";

                    createOds(task, reportFileOds, reportTmpFileOds).setHandler(createOds -> {
                        if (createOds.succeeded()) {
                            log.info("Created ODS report for language [{}]", task.getLanguageCode());
                        } else {
                            log.error("Failed to generate ODS report", createOds.cause());
                            vertx.fileSystem().delete(reportTmpFileOds, deleteFile -> {
                                if (deleteFile.failed())
                                    log.error("Failed to remove ODS tmp file", deleteFile.cause());
                            });
                        }
                    });
                } else {
                    log.error("Failed to generate XLSX report", createXlsx.cause());
                    vertx.fileSystem().delete(reportTmpFileXlsx, deleteFile -> {
                        if (deleteFile.failed())
                            log.error("Failed to remove XLSX tmp file", deleteFile.cause());
                    });
                }
            });
        });

        Promise<Void> readTranslations = Promise.promise();
        vertx.fileSystem().readFile("i18n/lang.json", readFile -> {
            if (readFile.succeeded()) {
                translations = new JsonObject(readFile.result());
                readTranslations.complete();
            } else {
                readTranslations.fail(readFile.cause());
            }
        });

        Promise<Void> readCountries = Promise.promise();
        vertx.fileSystem().readFile("i18n/countries.json", readFile -> {
            if (readFile.succeeded()) {
                countries = new JsonObject(readFile.result());
                readCountries.complete();
            } else {
                readCountries.fail(readFile.cause());
            }
        });

        CompositeFuture.all(readTranslations.future(), readCountries.future()).setHandler(handler -> {
            if (handler.succeeded()) {
                startPromise.complete();
            } else {
                startPromise.fail(handler.cause());
            }
        });
    }

    private Future<Void> createXlsx(ReportTask task, String reportFile, String reportTmpFile) {
        return Future.future(createXlsx -> {
            vertx.fileSystem().createFile(reportTmpFile, createFile -> {
                if (createFile.succeeded()) {
                    ReportLabelCreator reportLabelCreator = new ReportLabelCreator(task.getLanguageCode(), translations, countries);

                    List<Catalogue> catalogues = new ArrayList<>();
                    for (JsonObject c : task.getCatalogues()) {
                        Catalogue catalogue = new Catalogue(c);
                        PdfUtils.extractSectionsFromJson(catalogue, reportLabelCreator, new JsonObject(), new JsonObject());
                        catalogues.add(catalogue);
                    }
                    log.debug("Generating XLS for [{}] catalogues", catalogues.size());

                    vertx.executeBlocking(createXlsReport -> {
                                XlsReport xlsReport = new XlsReport(catalogues, task.getGlobalDashboard(), reportLabelCreator, reportTmpFile);
                                xlsReport.createXlsReport();
                                createXlsReport.complete();
                            }, result ->
                                    vertx.fileSystem().move(reportTmpFile, reportFile, new CopyOptions().setReplaceExisting(true), moveTmpFile -> {
                                        if (moveTmpFile.succeeded()) {
                                            createXlsx.complete();
                                        } else {
                                            createXlsx.fail(moveTmpFile.cause());
                                        }
                                    }));
                } else {
                    createXlsx.fail(createFile.cause());
                }
            });
        });
    }


    private Future<Void> createOds(ReportTask task, String reportFile, String reportTmpFile) {
        return Future.future(createOds -> {
            vertx.fileSystem().createFile(reportTmpFile, createFile -> {
                if (createFile.succeeded()) {
                    vertx.executeBlocking(convertXlsxToOds -> {
                        try (FileInputStream xlsxInput = new FileInputStream(task.getPath() + ".xlsx")) {

                            XSSFWorkbook xlsReport = new XSSFWorkbook(xlsxInput);

                            // get row and column counts
                            int maxRowCount = 0;
                            int maxColumnCount = 0;
                            for (org.apache.poi.ss.usermodel.Sheet sheet : xlsReport) {
                                maxRowCount = Math.max(sheet.getLastRowNum() + 1, maxRowCount);

                                for (Row row : sheet)
                                    maxColumnCount = row.getLastCellNum() > maxColumnCount ? row.getLastCellNum() : maxColumnCount;
                            }

                            // create ods workbook
                            SpreadSheet odsReport = SpreadSheet.create(xlsReport.getNumberOfSheets(), maxColumnCount, maxRowCount);

                            // copy each sheet to ods file
                            for (int sheetNumber = 0; sheetNumber < xlsReport.getNumberOfSheets(); sheetNumber++) {
                                org.apache.poi.ss.usermodel.Sheet xlsSheet = xlsReport.getSheetAt(sheetNumber);
                                Sheet odsSheet = odsReport.getSheet(sheetNumber);
                                odsSheet.setName(xlsSheet.getSheetName());

                                for (Row row : xlsSheet) {
                                    for (int columnNumber = row.getFirstCellNum(); columnNumber < row.getLastCellNum(); columnNumber++) {
                                        // check if call has String or Numeric value and copy cell value to according xls cell
                                        if (row.getCell(columnNumber).getCellType() == CellType.NUMERIC) {
                                            odsSheet.setValueAt(row.getCell(columnNumber).getNumericCellValue(), columnNumber, row.getRowNum());
                                        } else {
                                            odsSheet.setValueAt(row.getCell(columnNumber).getStringCellValue(), columnNumber, row.getRowNum());
                                        }
                                    }
                                }
                            }

                            writeOdsFile(odsReport, reportTmpFile, reportFile).setHandler(writeOds -> {
                                if (writeOds.succeeded()) {
                                    createOds.complete();
                                } else {
                                    createOds.fail(writeOds.cause());
                                }
                            });

                        } catch (IOException e) {
                            convertXlsxToOds.fail(e);
                        }
                    }, createOds);
                } else {
                    createOds.fail(createFile.cause());
                }
            });
        });
    }

    private Future<Void> writeOdsFile(SpreadSheet spreadSheet, String tmpFile, String reportFile) {
        return Future.future(writeOdsFile -> {
            try (FileOutputStream odsOutput = new FileOutputStream(tmpFile); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                spreadSheet.getPackage().save(out);
                byte[] odsReportAsByteArray = out.toByteArray();
                odsOutput.write(odsReportAsByteArray);
            } catch (IOException e) {
                writeOdsFile.fail(e);
            }

            vertx.fileSystem().move(tmpFile, reportFile, new CopyOptions().setReplaceExisting(true), moveTmpFile -> {
                if (moveTmpFile.succeeded()) {
                    writeOdsFile.complete();
                } else {
                    writeOdsFile.fail(moveTmpFile.cause());
                }
            });
        });
    }
}

