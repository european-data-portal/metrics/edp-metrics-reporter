package io.piveau.metrics.reporter;

import io.vertx.core.json.JsonArray;

import java.util.List;

public final class ApplicationConfig {

    static final String ENV_APPLICATION_PORT = "PORT";
    static final Integer DEFAULT_APPLICATION_PORT = 8080;

    static final String ENV_QUICKCHART_ADDRESS = "QUICKCHART_ADDRESS";
    static final String DEFAULT_QUICKCHART_ADDRESS = "http://quickchart-edp2.okd.fokus.fraunhofer.de:80/chart";

    public static final String ENV_METRICS_PORT = "METRICS_PORT";
    public static final Integer DEFAULT_METRICS_PORT = 80;

    public static final String ENV_METRICS_HOST = "METRICS_HOST";
    public static final String DEFAULT_METRICS_HOST =  "europeandataportal.eu";

    // TODO make dataset dynamic or separately configurable
    public static final String ENV_HUB_ADDRESS = "HUB_ADDRESS";
    public static final String DEFAULT_HUB_ADDRESS = "https://piveau-hub-edp2.okd.fokus.fraunhofer.de/datasets/test?catalogue=edp&data=true";

    public static final String ENV_LANGUAGES = "LANGUAGES";
//    public static final JsonArray DEFAULT_LANGUAGES = new JsonArray(List.of("bg", "cs"));
    public static final JsonArray DEFAULT_LANGUAGES = new JsonArray(List.of("bg", "cs", "da", "de", "el", "en", "es", "et", "fi", "fr", "ga", "hr", "hu", "it", "lt", "lv", "mt", "nl", "no", "pl", "pt", "ro", "sk", "sl", "sv"));

    // defaults don't make sense here
    public static final String ENV_API_KEY_HUB = "API_KEY_HUB";
    public static final String API_KEY_HUB = "";
    public static final String ENV_API_KEY_UPLOAD = "API_KEY_UPLOAD";
    public static final String API_KEY_UPLOAD = "";
}
