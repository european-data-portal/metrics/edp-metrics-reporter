package io.piveau.metrics.reporter.model;

import io.piveau.metrics.reporter.model.charts.ChartBundle;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;


public class Section {
    private String heading;
    private List<ChartBundle> chartBundles;
    private JsonObject sectionData;
    private JsonObject doughnut;
    private JsonObject bar;
    private String chartErrorMessage;


    public Section(JsonObject sectionData, JsonObject doughnut, JsonObject bar, String heading, String chartErrorMessage) {
        this.heading = heading;
        this.chartBundles = new ArrayList<>();
        this.sectionData = sectionData;
        this.doughnut = doughnut;
        this.bar = bar;
        this.chartErrorMessage = chartErrorMessage;
    }

    public String getSectionHeading() {
        return this.heading;
    }

    public void setSectionHeading(String heading) {
        this.heading = heading;
    }

    public JsonObject getSectionData() {
        return this.sectionData;
    }

    public void setSectionData(JsonObject sectionData) {
        this.sectionData = sectionData;
    }

    public List<ChartBundle> getChartBundles() {
        return this.chartBundles;
    }

    public String getChartErrorMessage() {
        return chartErrorMessage;
    }

    public void setChartErrorMessage(String chartErrorMessage) {
        this.chartErrorMessage = chartErrorMessage;
    }

    public JsonObject getDoughnut() {
        return doughnut;
    }

    public void setDoughnut(JsonObject doughnut) {
        this.doughnut = doughnut;
    }

    public JsonObject getBar() {
        return bar;
    }

    public void setBar(JsonObject bar) {
        this.bar = bar;
    }

    public void addChartBundle(ChartBundle chartBundle){
        this.chartBundles.add(chartBundle);
    }
}