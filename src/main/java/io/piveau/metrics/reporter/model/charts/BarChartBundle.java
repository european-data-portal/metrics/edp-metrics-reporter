package io.piveau.metrics.reporter.model.charts;

import com.itextpdf.layout.element.Image;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class stores all info needed for Bar Chart creation in one data structure:
 * - chartName of the chart
 * - data to insert into template
 * - Quickchart template (see insertIntoTemplate())
 * - chart image once it has been received by Quickchart
 * - boolean if no data for the chart is available and a matching error text
 */

public class BarChartBundle implements ChartBundle {

    private String chartName;
    private JsonArray data;
    private JsonObject template;
    private Image image;
    private boolean hasError;
    private String errorMessage;


    public BarChartBundle(String chartName, JsonArray data, JsonObject template, boolean hasError, String errorMessage) {
        this.chartName = chartName;
        this.data = data;
        this.template = template;
        this.image = null;
        this.hasError = hasError;
        this.errorMessage = errorMessage;
    }


    public String getChartName() {
        return this.chartName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    public Image getImage(){
        return this.image;
    }
    
    public void setImage(Image image) {
        this.image = image;
    }

    public String getErrorMessage(){
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean getHasError() {
        return this.hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public JsonObject getChartTemplate() {
        this.insertIntoTemplate();
        return this.template;
    }
    
    public void insertIntoTemplate() {
        if (!this.hasError) {
            List<String> dataKeyArray = new ArrayList<>();
            List<Integer> dataValueArray = new ArrayList<>();

            data.forEach(item -> {
                JsonObject obj = (JsonObject) item;
                int percentage = (int) Math.floor(obj.getDouble("percentage"));

                if (percentage >= 1) {
                    dataKeyArray.add(obj.getString("name"));
                    dataValueArray.add(percentage);
                }
            });

            JsonArray stringArray = new JsonArray(dataKeyArray);
            JsonArray intArray = new JsonArray(dataValueArray);

            template.getJsonObject("chart").getJsonObject("data").getJsonArray("labels").clear().addAll(stringArray);

            template.getJsonObject("chart").getJsonObject("data").getJsonArray("datasets").getJsonObject(0).getJsonArray("data")
                    .clear().addAll(intArray);

            //template.getJsonObject("chart").getJsonObject("options").getJsonObject("title").remove("text");
            //template.getJsonObject("chart").getJsonObject("options").getJsonObject("title").put("text", this.chartName);
        }
    }
}