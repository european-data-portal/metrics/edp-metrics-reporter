package io.piveau.metrics.reporter.model;

import java.util.Locale;

public class ReportLabels {

    private Locale language;

    private String mqaTitle;
    private String mqaIntro;
    private String edpUrl;
    private String dashboardTitle;

    private String metricLabelYes;
    private String metricLabelNo;

    private String catalogueCountry;
    private String catalogueName;
    private String accessibilityAccessUrl;
    private String machineReadability;
    private String dCatApCompliance;
    private String knownLicences;
    private String accessibilityDownloadUrl;

    private String methodologyHeadline;
    private String methodologySubHeadline;
    private String methodologyIntroText;
    private String methodologyQualityHeadline;
    private String methodologyQualityMainText;
    private String methodologyScopeHeadline;
    private String methodologyScopeMainText;
    private String methodologyCoverHeadline;
    private String methodologyCoverMainText;
    private String methodologyProcessHeadline;
    private String methodologyProcessMainText;
    private String methodologyAssumptionsHeadline;
    private String methodologyAssumptionsMainText;
    private String methodologyDimensionsHeadline;
    private String methodologyDimensionsMainText;

    private String noDataError;
    private String mqaDescription;
    private String catalogueRating;

    private String notAvailable;
    private String pageTranslation;

    private String scoreExcellent;
    private String scoreGood;
    private String scoreBad;
    private String scoreJustEnough;

    private String methodologyCoverLinkText;



    public String getNavigationDashboard() {
        return navigationDashboard;
    }

    public void setNavigationDashboard(String navigationDashboard) {
        this.navigationDashboard = navigationDashboard;
    }

    private String navigationDashboard;

    public Locale getLanguage() {
        return language;
    }

    public void setLanguage(Locale language) {
        this.language = language;
    }

    public String getMqaTitle() {
        return mqaTitle;
    }

    public void setMqaTitle(String mqaTitle) {
        this.mqaTitle = mqaTitle;
    }

    public String getMqaIntro() {
        return mqaIntro;
    }

    public void setMqaIntro(String mqaIntro) {
        this.mqaIntro = mqaIntro;
    }

    public String getEdpUrl() {
        return edpUrl;
    }

    public void setEdpUrl(String edpUrl) {
        this.edpUrl = edpUrl;
    }

    public String getDashboardTitle() {
        return dashboardTitle;
    }

    public void setDashboardTitle(String dashboardTitle) {
        this.dashboardTitle = dashboardTitle;
    }

    public String getMetricLabelYes() {
        return metricLabelYes;
    }

    public void setMetricLabelYes(String metricLabelYes) {
        this.metricLabelYes = metricLabelYes;
    }

    public String getMetricLabelNo() {
        return metricLabelNo;
    }

    public void setMetricLabelNo(String metricLabelNo) {
        this.metricLabelNo = metricLabelNo;
    }

    public String getCatalogueCountry(){
        return catalogueCountry;
    }

    public void setCatalogueCountry(String catalogueCountry) {
        this.catalogueCountry = catalogueCountry;
    }

    public String getCatalogueName(){
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getAccessibilityAccessUrl(){
        return accessibilityAccessUrl;
    }

    public void setAccessibilityAccessUrl(String accessibilityAccessUrl) {
        this.accessibilityAccessUrl = accessibilityAccessUrl;
    }

    public String getMachineReadability(){
        return machineReadability;
    }

    public void setMachineReadability(String machineReadability) {
        this.machineReadability = machineReadability;
    }

    public String getDcatApCompliance(){
        return dCatApCompliance;
    }

    public void setDcatApCompliance(String dCatApCompliance) {
        this.dCatApCompliance = dCatApCompliance;
    }

    public String getKnownLicences(){
        return knownLicences;
    }

    public void setKnownLicences(String knownLicences) {
        this.knownLicences = knownLicences;
    }

    public String getAccessibilityDownloadUrl(){
        return accessibilityDownloadUrl;
    }

    public void setAccessibilityDownloadUrl(String accessibilityDownloadUrl) {
        this.accessibilityDownloadUrl = accessibilityDownloadUrl;
    }

    public String getMethodologyHeadline() {
        return methodologyHeadline;
    }

    public void setMethodologyHeadline(String methodologyHeadline) {
        this.methodologyHeadline = methodologyHeadline;
    }

    public String getMethodologySubHeadline() {
        return methodologySubHeadline;
    }

    public void setMethodologySubHeadline(String methodologySubHeadline) {
        this.methodologySubHeadline = methodologySubHeadline;
    }

    public String getMethodologyIntroText() {
        return methodologyIntroText;
    }

    public void setMethodologyIntroText(String methodologyIntroText) {
        this.methodologyIntroText = methodologyIntroText;
    }

    public String getMethodologyQualityHeadline() {
        return methodologyQualityHeadline;
    }

    public void setMethodologyQualityHeadline(String methodologyQualityHeadline) {
        this.methodologyQualityHeadline = methodologyQualityHeadline;
    }

    public String getMethodologyQualityMainText() {
        return methodologyQualityMainText;
    }

    public void setMethodologyQualityMainText(String methodologyQualityMainText) {
        this.methodologyQualityMainText = methodologyQualityMainText;
    }

    public String getMethodologyScopeHeadline() {
        return methodologyScopeHeadline;
    }

    public void setMethodologyScopeHeadline(String methodologyScopeHeadline) {
        this.methodologyScopeHeadline = methodologyScopeHeadline;
    }

    public String getMethodologyScopeMainText() {
        return methodologyScopeMainText;
    }

    public void setMethodologyScopeMainText(String methodologyScopeMainText) {
        this.methodologyScopeMainText = methodologyScopeMainText;
    }

    public String getMethodologyCoverHeadline() {
        return methodologyCoverHeadline;
    }

    public void setMethodologyCoverHeadline(String methodologyCoverHeadline) {
        this.methodologyCoverHeadline = methodologyCoverHeadline;
    }

    public String getMethodologyCoverMainText() {
        return methodologyCoverMainText;
    }

    public void setMethodologyCoverMainText(String methodologyCoverMainText) {
        this.methodologyCoverMainText = methodologyCoverMainText;
    }

    public String getMethodologyProcessHeadline() {
        return methodologyProcessHeadline;
    }

    public void setMethodologyProcessHeadline(String methodologyProcessHeadline) {
        this.methodologyProcessHeadline = methodologyProcessHeadline;
    }

    public String getMethodologyProcessMainText() {
        return methodologyProcessMainText;
    }

    public void setMethodologyProcessMainText(String methodologyProcessMainText) {
        this.methodologyProcessMainText = methodologyProcessMainText;
    }

    public String getMethodologyAssumptionsHeadline() {
        return methodologyAssumptionsHeadline;
    }

    public void setMethodologyAssumptionsHeadline(String methodologyAssumptionsHeadline) {
        this.methodologyAssumptionsHeadline = methodologyAssumptionsHeadline;
    }

    public String getMethodologyAssumptionsMainText() {
        return methodologyAssumptionsMainText;
    }

    public void setMethodologyAssumptionsMainText(String methodologyAssumptionsMainText) {
        this.methodologyAssumptionsMainText = methodologyAssumptionsMainText;
    }

    public String getMethodologyDimensionsHeadline() {
        return methodologyDimensionsHeadline;
    }

    public void setMethodologyDimensionsHeadline(String methodologyDimensionsHeadline) {
        this.methodologyDimensionsHeadline = methodologyDimensionsHeadline;
    }

    public String getMethodologyDimensionsMainText() {
        return methodologyDimensionsMainText;
    }

    public void setMethodologyDimensionsMainText(String methodologyDimensionsMainText) {
        this.methodologyDimensionsMainText = methodologyDimensionsMainText;
    }

    public String getNoDataError() {
        return noDataError;
    }

    public void setNoDataError(String noDataError) {
        this.noDataError = noDataError;
    }

    public String getMqaDescription() {
        return mqaDescription;
    }

    public void setMqaDescription(String mqaDescription){
        this.mqaDescription = mqaDescription;
    }

    public String getCatalogueRating() {
        return catalogueRating;
    }

    public void setCatalogueRating(String catalogueRating) {
        this.catalogueRating = catalogueRating;
    }

    public String getNotAvailable() {
        return notAvailable;
    }

    public void setNotAvailable(String notAvailable) {
        this.notAvailable = notAvailable;
    }

    public String getPageTranslation() {
        return pageTranslation;
    }

    public void setPageTranslation(String pageTranslation) {
        this.pageTranslation = pageTranslation;
    }

    public String getScoreExcellent() {
        return scoreExcellent;
    }

    public void setScoreExcellent(String scoreExcellent) {
        this.scoreExcellent = scoreExcellent;
    }

    public String getScoreGood() {
        return scoreGood;
    }

    public void setScoreGood(String scoreGood) {
        this.scoreGood = scoreGood;
    }

    public String getScoreBad() {
        return scoreBad;
    }

    public void setScoreBad(String scoreBad) {
        this.scoreBad = scoreBad;
    }

    public String getScoreJustEnough() {
        return scoreJustEnough;
    }

    public void setScoreJustEnough(String scoreJustEnough) {
        this.scoreJustEnough = scoreJustEnough;
    }

    public String getMethodologyCoverLinkText() {
        return methodologyCoverLinkText;
    }

    public void setMethodologyCoverLinkText(String methodologyCoverLinkText) {
        this.methodologyCoverLinkText = methodologyCoverLinkText;
    }
}
