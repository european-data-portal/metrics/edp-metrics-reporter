package io.piveau.metrics.reporter.model.charts;

import com.itextpdf.layout.element.Image;
import io.vertx.core.json.JsonObject;


public interface ChartBundle {

    String getChartName();

    Image getImage();

    void setImage(Image image);

    JsonObject getChartTemplate();

    boolean equals(Object obj);

    String getErrorMessage();

    boolean getHasError();
}
