package io.piveau.metrics.reporter.model;

import com.itextpdf.io.font.FontProgram;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.Style;

public class Styles {

    private PdfFont robotoRegular;
    private PdfFont robotoBold;

    private Style headingOneStyle;
    private Style headingTwoStyle;
    private Style headingThreeStyle;
    private Style headingFourStyle;
    private Style paragraphStyle;
    private Style boldParagraphStyle;
    private Style tableHeaderStyle;
    private Style tableBodyStyle;
    private Style footerStyle;

    public Styles(FontProgram regular, FontProgram bold) {
        this.robotoRegular = PdfFontFactory.createFont(regular, "Identity-H", true);
        this.robotoBold = PdfFontFactory.createFont(bold, "Identity-H", true);

        headingOneStyle = getStyle(robotoRegular, 18);
        headingTwoStyle = getStyle(robotoRegular, 16);
        headingThreeStyle = getStyle(robotoBold, 14);
        headingFourStyle = getStyle(robotoRegular, 14);
        paragraphStyle = getStyle(robotoRegular, 12);
        boldParagraphStyle = getStyle(robotoBold, 12);
        tableHeaderStyle = getStyle(robotoBold, 10);
        tableBodyStyle = getStyle(robotoRegular, 10);
        footerStyle = getStyle(robotoRegular, 10).setItalic();
    }

    public Style getHeadingOneStyle() {
        return headingOneStyle;
    }

    public Style getHeadingTwoStyle() {
        return headingTwoStyle;
    }

    public Style getHeadingThreeStyle() {
        return headingThreeStyle;
    }

    public Style getHeadingFourStyle() {
        return headingFourStyle;
    }

    public Style getParagraphStyle() {
        return paragraphStyle;
    }

    public Style getBoldParagraphStyle() {
        return boldParagraphStyle;
    }

    public Style getTableHeaderStyle() {
        return tableHeaderStyle;
    }

    public Style getTableBodyStyle() {
        return tableBodyStyle;
    }

    public Style getFooterStyle() {
        return footerStyle;
    }

    public PdfFont getRobotoRegular() {
        return this.robotoRegular;
    }

    public PdfFont getRobotoBold() {
        return this.robotoBold;
    }

    private Style getStyle(PdfFont font, int fontSize){
        return new Style()
                .setFont(font)
                .setFontSize(fontSize);
    }
}
