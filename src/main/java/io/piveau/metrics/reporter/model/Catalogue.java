package io.piveau.metrics.reporter.model;

import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class Catalogue {

    private JsonObject metrics;

    private String id;
    private String title;
    private String description;
    private String spatial;
    private String type;
    private List<Section> sections;

    public Catalogue(JsonObject json) {
        this.metrics = json;
        this.sections = new ArrayList<>();

        JsonObject catalogueInfo = json.getJsonObject("info");
        this.id = catalogueInfo.getString("id");
        this.title = catalogueInfo.getString("title");
        this.description = catalogueInfo.getString("description");
        this.spatial = catalogueInfo.getString("spatial");
        this.type = catalogueInfo.getString("type");
    }

    public JsonObject getMetrics() {
        return metrics;
    }

    public void setMetrics(JsonObject metrics) {
        this.metrics = metrics;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String spatial) {
        this.spatial = spatial;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
