package io.piveau.metrics.reporter.model;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MetricsPdfDocument {

    private PdfDocument pdfDoc;
    private Document document;
    private List<AbstractMap.SimpleEntry<String, Integer>> toc;
    private Styles styles;
    private Locale locale;

    public MetricsPdfDocument(Locale locale, PdfWriter pdfWriter, Styles styles) throws IOException {
        this.pdfDoc = new PdfDocument(pdfWriter);
        this.pdfDoc.addNewPage();
        this.document = new Document(pdfDoc);
        this.toc = new ArrayList<>();
        this.styles = styles;
        this.locale = locale;
    }

    public PdfDocument getPdfDoc() {
        return pdfDoc;
    }

    public void setPdfDoc(PdfDocument pdfDoc) {
        this.pdfDoc = pdfDoc;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public List<AbstractMap.SimpleEntry<String, Integer>> getToc() {
        return toc;
    }

    public void setToc(List<AbstractMap.SimpleEntry<String, Integer>> toc) {
        this.toc = toc;
    }

    public Styles getStyles() {
        return styles;
    }

    public void setStyles(Styles styles) {
        this.styles = styles;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

}
