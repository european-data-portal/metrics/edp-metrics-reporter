package io.piveau.metrics.reporter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Locale;

public class ReportTask {
    private Locale languageCode;
    private String reportDir;
    private JsonArray globalDashboard;
    private List<JsonObject> catalogues;

    // required by Json encode/decode
    public ReportTask() {}

    public ReportTask(Locale languageCode, String reportDir, JsonArray globalDashboard, List<JsonObject> catalogues) {
        this.languageCode = languageCode;
        this.reportDir = reportDir;
        this.globalDashboard = globalDashboard;
        this.catalogues = catalogues;
    }

    public Locale getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(Locale languageCode) {
        this.languageCode = languageCode;
    }

    public String getReportDir() {
        return reportDir;
    }

    public void setReportDir(String reportDir) {
        this.reportDir = reportDir;
    }

    public JsonArray getGlobalDashboard() {
        return globalDashboard;
    }

    public void setGlobalDashboard(JsonArray globalDashboard) {
        this.globalDashboard = globalDashboard;
    }

    public List<JsonObject> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(List<JsonObject> catalogues) {
        this.catalogues = catalogues;
    }

    @JsonIgnore
    public String getPath() {
        return reportDir + "/" + languageCode.toString();
    }
}
