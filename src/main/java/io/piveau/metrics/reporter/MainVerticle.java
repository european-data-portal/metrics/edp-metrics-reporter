package io.piveau.metrics.reporter;

import io.piveau.metrics.reporter.model.ReportTask;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static io.piveau.metrics.reporter.ApplicationConfig.*;


public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;
    private WebClient webClient;


    @Override
    public void start(Promise<Void> startPromise) {
        Future<Void> steps = loadConfig()
                .compose(config -> bootstrapVerticles())
                .compose(bootstrap -> createReportDir())
                .compose(this::startServer);

        steps.setHandler(handler -> {
            if (handler.succeeded()) {
                log.info("Metrics Reporter successfully launched");
                startPromise.complete();
            } else {
                log.error("Failed to launch Metrics Reporter: " + handler.cause());
                startPromise.fail(handler.cause());
                vertx.close();
            }
        });
    }

    private Future<JsonObject> loadConfig() {
        return Future.future(loadConfig ->
                ConfigRetriever.create(vertx).getConfig(handler -> {
                    if (handler.succeeded()) {
                        config = handler.result();
                        log.debug(config.encodePrettily());
                        loadConfig.complete(handler.result());
                    } else {
                        loadConfig.fail("Failed to load config: " + handler.cause());
                    }
                }));
    }

    private Future<Router> prepareRouter(String reportDir) {
        WebClientOptions clientOptions = new WebClientOptions()
                .setDefaultHost(config().getString(ENV_METRICS_HOST, DEFAULT_METRICS_HOST))
                .setDefaultPort(config().getInteger(ENV_METRICS_PORT, DEFAULT_METRICS_PORT));

        webClient = WebClient.create(vertx, clientOptions);

        return Future.future(setRoutes ->
                OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", ar -> {
                    if (ar.succeeded()) {
                        OpenAPI3RouterFactory routerFactory = ar.result();

                        routerFactory.addHandlerByOperationId("generate", routingContext ->
                                generateReports(routingContext, reportDir));

                        routerFactory.addHandlerByOperationId("upload", routingContext -> {
                            vertx.eventBus().send(UploadVerticle.ADDRESS, reportDir);
                            routingContext.response().setStatusCode(202).end();
                        });

                        /** Shift handling of getting reports to frontend */
                        routerFactory.addHandlerByOperationId("getReport", routingContext -> {
                            String languageCode = routingContext.pathParams().get("languageCode");
                            switch (routingContext.pathParams().get("format")) {
                                case "pdf":
                                    vertx.fileSystem().readFile(reportDir + "/" + languageCode + ".pdf", readPdf -> {
                                        if (readPdf.succeeded()) {
                                            routingContext.response()
                                                    .setChunked(true)
                                                    .putHeader("Content-Type", "application/pdf")
                                                    .putHeader("Content-Disposition", "attachment; filename=\"EDP2-Metrics-Report.pdf\"")
                                                    .setStatusCode(200).end(readPdf.result());
                                        } else {
                                            log.error("Failed to read file", readPdf.cause());
                                            routingContext.response().setStatusCode(500).end();
                                        }
                                    });
                                    break;
                                case "ods":
                                    vertx.fileSystem().readFile(reportDir + "/" + languageCode + ".ods", readOds -> {
                                        if (readOds.succeeded()) {
                                            routingContext.response()
                                                    .setChunked(true)
                                                    .putHeader("Content-Type", "application/vnd.oasis.opendocument.spreadsheet")
                                                    .putHeader("Content-Disposition", "attachment; filename=\"EDP2-Metrics-Report.ods\"")
                                                    .setStatusCode(200).end(readOds.result());
                                        } else {
                                            routingContext.response().setStatusCode(500).end();
                                        }
                                    });
                                    break;
                                case "xlsx":
                                    vertx.fileSystem().readFile(reportDir + "/" + languageCode + ".xlsx", readXlsx -> {
                                        if (readXlsx.succeeded()) {
                                            routingContext.response()
                                                    .setChunked(true)
                                                    .putHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                                                    .putHeader("Content-Disposition", "attachment; filename=\"EDP2-Metrics-Report.xlsx\"")
                                                    .setStatusCode(200).end(readXlsx.result());
                                        } else {
                                            routingContext.response().setStatusCode(500).end();
                                        }
                                    });
                                    break;
                            }
                        });

                        setRoutes.complete(routerFactory.getRouter());

                    } else {
                        setRoutes.fail(ar.cause());
                    }
                }));
    }

    private void generateReports(RoutingContext context, String reportDir) {

        getGlobalDashboard().setHandler(globalDashboard -> {
            if (globalDashboard.succeeded()) {
                List<String> catalogueIds = new ArrayList<>();

                for (Object catalogue : globalDashboard.result())
                    catalogueIds.add(((JsonObject) catalogue).getJsonObject("info").getString("id"));

                List<Future> catalogueFutures = catalogueIds.stream()
                        .map(this::getCatalogue)
                        .collect(Collectors.toList());

                CompositeFuture.all(catalogueFutures).setHandler(fetchCatalogues -> {
                    if (fetchCatalogues.succeeded()) {
                        List<JsonObject> catalogues = catalogueFutures.stream()
                                .map(future -> (JsonObject) future.result())
                                .collect(Collectors.toList());

                        config.getJsonArray(ENV_LANGUAGES, DEFAULT_LANGUAGES).forEach(languageCode -> {
                            ReportTask task = new ReportTask(new Locale((String) languageCode), reportDir, globalDashboard.result(), catalogues);
                            vertx.eventBus().send(PdfCreatorVerticle.ADDRESS, Json.encode(task));
                            vertx.eventBus().send(TableReportVerticle.ADDRESS, Json.encode(task));
                        });
                    } else {
                        log.error("Failed to fetch catalogues", fetchCatalogues.cause());
                    }
                });
            } else {
                log.error("Failed to fetch global dashboard metrics", globalDashboard.cause());
            }
        });

        context.response().setStatusCode(202).end();
    }

    private Future<JsonArray> getGlobalDashboard() {
        return Future.future(fetchDashboard ->
                webClient.get("/api/mqa/cache/info/catalogues")
                        .expect(ResponsePredicate.SC_OK)
                        .send(fetchMetrics -> {
                            if (fetchMetrics.succeeded()) {
                                fetchDashboard.complete(fetchMetrics.result().bodyAsJsonArray());
                            } else {
                                fetchDashboard.fail(fetchMetrics.cause());
                            }
                        }));
    }

    private Future<JsonObject> getCatalogue(String catalogueId) {
        return Future.future(fetchCatalogue ->
                webClient.get("/api/mqa/cache/metrics/catalogues/" + catalogueId)
                        .expect(ResponsePredicate.SC_OK)
                        .send(fetchMetrics -> {
                            if (fetchMetrics.succeeded()) {
                                fetchCatalogue.complete(fetchMetrics.result().bodyAsJsonObject());
                            } else {
                                fetchCatalogue.fail(fetchMetrics.cause());
                            }
                        }));
    }

    private Future<String> createReportDir() {
        return Future.future(createReportDir ->
                vertx.fileSystem().createTempDirectory("reports", createReportDir));
    }

    private Future<Void> startServer(String reportDir) {
        return Future.future(startServer ->
                prepareRouter(reportDir).setHandler(handleRouter -> {
                    if (handleRouter.succeeded()) {
                        Router router = handleRouter.result();
                        vertx.createHttpServer(new HttpServerOptions()
                                .setPort(config.getInteger(ApplicationConfig.ENV_APPLICATION_PORT, ApplicationConfig.DEFAULT_APPLICATION_PORT)))
                                .requestHandler(router).listen();

                        startServer.complete();
                    } else {
                        startServer.fail(handleRouter.cause());
                    }
                }));
    }

    private CompositeFuture bootstrapVerticles() {
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(config)
                .setWorker(true);

        List<Future> deploymentFutures = new ArrayList<>();
        deploymentFutures.add(deployVerticle(options, PdfCreatorVerticle.class));
        deploymentFutures.add(deployVerticle(options, TableReportVerticle.class));
        deploymentFutures.add(deployVerticle(options, UploadVerticle.class));

        return CompositeFuture.join(deploymentFutures);
    }

    private Future<String> deployVerticle(DeploymentOptions options, Class<? extends AbstractVerticle> clazz) {
        return Future.future(deployVerticle ->
                vertx.deployVerticle(clazz, options, deployVerticle));
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }
}
