package io.piveau.metrics.reporter;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.navigation.PdfExplicitDestination;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.font.FontProvider;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import io.piveau.metrics.reporter.model.*;
import io.piveau.metrics.reporter.model.charts.ChartBundle;
import io.piveau.metrics.reporter.utils.PdfUtils;
import io.piveau.metrics.reporter.utils.ReportLabelCreator;
import io.piveau.metrics.reporter.utils.TableCreator;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static io.piveau.metrics.reporter.ApplicationConfig.DEFAULT_QUICKCHART_ADDRESS;
import static io.piveau.metrics.reporter.ApplicationConfig.ENV_QUICKCHART_ADDRESS;


public class PdfCreatorVerticle extends AbstractVerticle {

    public static final String ADDRESS = "pdf-message-receiver";

    private static final Logger log = LoggerFactory.getLogger(PdfCreatorVerticle.class);

    private WebClient client;

    private FontProgram robotoRegular;
    private FontProgram robotoBold;
    private Styles styles;

    private JsonObject doughnut;
    private JsonObject bar;

    private JsonObject translations;
    private JsonObject countries;

    private int catalogueCount = 0;


    @Override
    public void start(Promise<Void> startPromise) {

        client = WebClient.create(vertx);

        vertx.eventBus().consumer(ADDRESS, message -> {
            // regular JSON decode() / mapTo() doesn't work
            JsonObject received = new JsonObject(message.body().toString());
            ReportTask task = new ReportTask(
                    new Locale(received.getString("languageCode")),
                    received.getString("reportDir"),
                    received.getJsonArray("globalDashboard"),
                    received.getJsonArray("catalogues").stream()
                            .map(entry -> (JsonObject) entry)
                            .collect(Collectors.toList())
            );

            String reportFile = task.getPath() + ".pdf";
            String reportTmpFile = reportFile + ".tmp";

            createPdf(task, reportFile, reportTmpFile).setHandler(createPdf -> {
                if (createPdf.failed()) {
                    log.error("Failed to generate PDF for language {}", task.getLanguageCode(), createPdf.cause());

                    vertx.fileSystem().delete(reportTmpFile, deleteFile -> {
                        if (deleteFile.failed())
                            log.error("Failed to remove tmp file", deleteFile.cause());
                    });
                }
            });
        });

        List<Future> readFiles = new ArrayList<>();

        Future<Buffer> readDoughnut = readFileFromResources("charts/doughnut.json");
        readFiles.add(readDoughnut);

        Future<Buffer> readBar = readFileFromResources("charts/bar.json");
        readFiles.add(readBar);

        Future<Buffer> readTranslations = readFileFromResources("i18n/lang.json");
        readFiles.add(readTranslations);

        Future<Buffer> readCountries = readFileFromResources("i18n/countries.json");
        readFiles.add(readCountries);

        CompositeFuture.all(readFiles).setHandler(handler -> {
            if (handler.succeeded()) {
                try {
                    doughnut = readDoughnut.result().toJsonObject();
                    bar = readBar.result().toJsonObject();
                    translations = readTranslations.result().toJsonObject();
                    countries = readCountries.result().toJsonObject();

                    robotoRegular = FontProgramFactory.createFont("src/main/resources/fonts/Roboto-Regular.ttf");
                    robotoBold = FontProgramFactory.createFont("src/main/resources/fonts/Roboto-Bold.ttf");

                    startPromise.complete();
                } catch (IOException e) {
                    startPromise.fail(e);
                }
            } else {
                startPromise.fail(handler.cause());
            }
        });
    }

    /*
     * Method combines paragraphs, tables and charts into a PDF file
     */
    private Future<Void> createPdf(ReportTask task, String reportFile, String reportTmpFile) {
        log.info("Generating PDF report for language [{}]", task.getLanguageCode());

        return Future.future(createPdf ->
                vertx.fileSystem().createFile(reportTmpFile, createFile -> {
                    if (createFile.succeeded()) {
                        try {
                            PdfWriter pdfWriter = new PdfWriter(reportTmpFile, new WriterProperties().addXmpMetadata().setPdfVersion(PdfVersion.PDF_1_7));
                            styles = new Styles(robotoRegular, robotoBold);

                            ReportLabelCreator reportLabelCreator = new ReportLabelCreator(task.getLanguageCode(), translations, countries);
                            ReportLabels reportLabels = reportLabelCreator.getReportLabels();
                            MetricsPdfDocument metricsPdfDocument = new MetricsPdfDocument(task.getLanguageCode(), pdfWriter, styles);

                            metricsPdfDocument.getPdfDoc().addNewPage();

                            // adding footer
                            PageNumber pageNumberEvent = new PageNumber(metricsPdfDocument, reportLabels);
                            metricsPdfDocument.getPdfDoc().addEventHandler(PdfDocumentEvent.END_PAGE, pageNumberEvent);

                            // adding title page
                            addTitlePage(metricsPdfDocument, reportLabels);

                            // adding methodology page
                            addMethodologyPage(metricsPdfDocument, reportLabels);

                            // add table with global metrics
                            metricsPdfDocument.getDocument().add(new AreaBreak((AreaBreakType.NEXT_PAGE)));

                            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getNavigationDashboard(),
                                    metricsPdfDocument.getStyles().getHeadingOneStyle()));
                            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getNavigationDashboard(),
                                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
                            setBlueStroke(metricsPdfDocument, false);
                            metricsPdfDocument.getDocument().add(new Paragraph(new Text("\n")));

                            Table table = getTable(task, reportLabelCreator);
                            metricsPdfDocument.getDocument().add(table);

                            // iterate over catalogue IDs
                            List<Future> createCatalogues = task.getCatalogues().stream()
                                    .map(catalogue ->
                                            // create Catalogue object from received file from Metrics
                                            createCatalogue(metricsPdfDocument, catalogue, reportLabelCreator)
                                    ).collect(Collectors.toList());

                            CompositeFuture.all(createCatalogues).setHandler(handler -> {
                                if (handler.succeeded()) {
                                    log.debug("Catalogues have been extracted");
                                    //TOC Creation
                                    metricsPdfDocument.getDocument().add(new AreaBreak((AreaBreakType.NEXT_PAGE)));
                                    //int startToc = pdfDoc.getNumberOfPages();
                                    metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph("Table Of Contents",
                                            metricsPdfDocument.getStyles().getHeadingOneStyle()));

                                    List<TabStop> tabStops = new ArrayList<>();
                                    tabStops.add(new TabStop(580, TabAlignment.RIGHT, new DottedLine()));

                                    for (AbstractMap.SimpleEntry<String, Integer> entry : metricsPdfDocument.getToc()) {
                                        Paragraph tocParagraph = new Paragraph()
                                                .addStyle(metricsPdfDocument.getStyles().getParagraphStyle())
                                                .addTabStops(tabStops)
                                                .add(entry.getKey())
                                                .add(new Tab())
                                                .add(String.valueOf(entry.getValue()))
                                                .setAction(PdfAction.createGoTo(PdfExplicitDestination.createFit(metricsPdfDocument.getPdfDoc().getPage(entry.getValue()))));
                                        metricsPdfDocument.getDocument().add(tocParagraph);
                                    }

                                    metricsPdfDocument.getDocument().close();
                                    metricsPdfDocument.getPdfDoc().close();

                                    vertx.fileSystem().move(reportTmpFile, reportFile, new CopyOptions().setReplaceExisting(true), moveTmpFile -> {
                                        if (moveTmpFile.succeeded()) {
                                            log.info("Created PDF for language [{}]", task.getLanguageCode());
                                            createPdf.complete();
                                        } else {
                                            createPdf.fail(moveTmpFile.cause());
                                        }
                                    });
                                } else {
                                    createPdf.fail(handler.cause());
                                }
                            });
                        } catch (Exception e) {
                            createPdf.fail(e);
                        }
                    } else {
                        createPdf.fail(createFile.cause());
                    }
                }));
    }


    /**
     * Method creates and adds a title page to the document
     */
    private void addTitlePage(MetricsPdfDocument metricsPdfDocument, ReportLabels reportLabels) {
        try {
            metricsPdfDocument.getDocument().add(new Image(ImageDataFactory.create("src/main/resources/mqa_report_header.png")));
        } catch (MalformedURLException e) {
            log.error("Failed to read mqa report header file", e);
        }

        //adding table so content is centered on the page
        Table titlePageTable = new Table(1).useAllAvailableWidth();
        titlePageTable.addCell(PdfUtils.getCellWithCenteredText(reportLabels.getEdpUrl(), metricsPdfDocument.getStyles().getHeadingTwoStyle()));
        titlePageTable.addCell(PdfUtils.getCellWithCenteredText(reportLabels.getMqaTitle(), metricsPdfDocument.getStyles().getHeadingOneStyle()));

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        titlePageTable.addCell(PdfUtils.getCellWithCenteredText(formatter.format(date), metricsPdfDocument.getStyles().getParagraphStyle()));
        metricsPdfDocument.getDocument().add(titlePageTable);

        metricsPdfDocument.getDocument().add(new AreaBreak((AreaBreakType.NEXT_PAGE)));
    }


    /**
     * Method inserts the methodology page to the document
     */
    private void addMethodologyPage(MetricsPdfDocument metricsPdfDocument, ReportLabels reportLabels) {
        try {
            ConverterProperties properties = new ConverterProperties();
            FontProvider fontProvider = new DefaultFontProvider(false, false, false);
            fontProvider.addFont(styles.getRobotoRegular().getFontProgram());
            fontProvider.addFont(styles.getRobotoBold().getFontProgram());
            properties.setFontProvider(fontProvider);

            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getMethodologyHeadline(),
                    metricsPdfDocument.getStyles().getHeadingOneStyle()));
            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getMethodologyHeadline(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));

            //adding blue stroke below headline
            setBlueStroke(metricsPdfDocument, false);
            metricsPdfDocument.getDocument().add(new Paragraph(new Text("\n")));

            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getMethodologySubHeadline(),
                    metricsPdfDocument.getStyles().getHeadingTwoStyle()));
            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getMethodologySubHeadline(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
            List<IElement> mqaDescriptionHTML = HtmlConverter.convertToElements(reportLabels.getMqaIntro(), properties);
            addHTMLElementsToDocument(metricsPdfDocument, mqaDescriptionHTML);
            String mqaLink = "<a href=\"https://europeandataportal.eu/mqa/methodology\" target=\"_blank\">" + reportLabels.getMethodologyHeadline() + "</a>";
            addHTMLElementsToDocument(metricsPdfDocument, HtmlConverter.convertToElements(mqaLink, properties));
            metricsPdfDocument.getDocument().add(new Paragraph(new Text("\n")));

            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getMethodologyScopeHeadline(),
                    metricsPdfDocument.getStyles().getHeadingThreeStyle()));
            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getMethodologyScopeHeadline(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
            List<IElement> methodologyScopeMainTextHTML = HtmlConverter.convertToElements(reportLabels.getMethodologyScopeMainText(), properties);
            addHTMLElementsToDocument(metricsPdfDocument, methodologyScopeMainTextHTML);

            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getMethodologyCoverHeadline(),
                    metricsPdfDocument.getStyles().getHeadingThreeStyle()));
            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getMethodologyCoverHeadline(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
            List<IElement> methodologyCoverMainTextHTML = HtmlConverter.convertToElements(reportLabels.getMethodologyCoverMainText(), properties);
            addHTMLElementsToDocument(metricsPdfDocument, methodologyCoverMainTextHTML);

            String shaclLink = "<a href=\"https://europeandataportal.eu/shacl/\" target=\"_blank\">" + reportLabels.getMethodologyCoverLinkText() + "</a>";
            addHTMLElementsToDocument(metricsPdfDocument, HtmlConverter.convertToElements(shaclLink, properties));
            metricsPdfDocument.getDocument().add(new Paragraph(new Text("\n")));

            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(reportLabels.getMethodologyAssumptionsHeadline(),
                    metricsPdfDocument.getStyles().getHeadingThreeStyle()));
            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(reportLabels.getMethodologyAssumptionsHeadline(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
            List<IElement> methodologyAssumptionsMainText = HtmlConverter.convertToElements(reportLabels.getMethodologyAssumptionsMainText(),
                    properties);

            for (IElement element : methodologyAssumptionsMainText) {
                // Elements with <h1>-<h6> HTML tags are transformed instances of the div Class by iText. Therefore, the
                // Paragraph element must be extracted from the list of the div's children - only then the correct style can be applied
                if (element.getClass().getName().equals("com.itextpdf.layout.element.Div")) {
                    Div div = (Div) element;
                    div.addStyle(styles.getParagraphStyle());
                    List<IElement> divElementsList = div.getChildren();
                    for (IElement divElement : divElementsList) {
                        Paragraph p = (Paragraph) divElement;
                        metricsPdfDocument.getDocument().add(p.setFont(metricsPdfDocument.getStyles().getRobotoRegular()).setUnderline());
                        //metricsPdfDocument.getDocument().add(p.addStyle(metricsPdfDocument.getStyles().getParagraphStyle()).setUnderline());
                        //metricsPdfDocument.getDocument().add(p.addStyle(styles.getParagraphStyle()).setUnderline());
                    }
                } else {
                    metricsPdfDocument.getDocument().add((IBlockElement) element);
                }
            }
        } catch (IOException e) {
            log.error("Failed to create methodology page for language [{}]", metricsPdfDocument.getLocale().toString(), e);
        }
    }


    /**
     * Helper Method to extract HTML Elements from List<IElement> and add them to the document
     *
     * @param htmlElements
     */
    private void addHTMLElementsToDocument(MetricsPdfDocument metricsPdfDocument, List<IElement> htmlElements) {
        for (IElement element : htmlElements) {
            metricsPdfDocument.getDocument().add((IBlockElement) element);
        }
    }


    /**
     * Method adds Section Headings and charts per Catalogue to the document
     */
    private void addCatalogueDataToDocument(MetricsPdfDocument metricsPdfDocument, Catalogue catalogue) {
        for (Section section : catalogue.getSections()) {
            metricsPdfDocument.getDocument().add(new AreaBreak((AreaBreakType.NEXT_PAGE)));
            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(catalogue.getTitle(), metricsPdfDocument.getStyles().getHeadingThreeStyle()));
            metricsPdfDocument.getDocument().add(PdfUtils.generateParagraph(section.getSectionHeading(), metricsPdfDocument.getStyles().getHeadingTwoStyle()));

            //adding blue stroke below headline
            setBlueStroke(metricsPdfDocument, true);

            metricsPdfDocument.getToc().add(new AbstractMap.SimpleEntry<>(catalogue.getTitle() + " - " + section.getSectionHeading(),
                    metricsPdfDocument.getPdfDoc().getNumberOfPages()));
            Table chartTable = new Table(UnitValue.createPercentArray(2)).useAllAvailableWidth();

            for (ChartBundle chartBundle : section.getChartBundles()) {
                Cell cell = PdfUtils.saveChartBundleHeadingAndImageToCell(chartBundle, metricsPdfDocument.getStyles());
                chartTable.addCell(cell);
            }

            metricsPdfDocument.getDocument().add(chartTable);
        }
    }

    /**
     * * Method instantiates TableCreator class to create an iText Table
     */
    private Table getTable(ReportTask task, ReportLabelCreator labelCreator) {
        ReportLabels reportLabels = labelCreator.getReportLabels();

        String[] headerCells = {reportLabels.getCatalogueCountry(),
                reportLabels.getCatalogueName(),
                reportLabels.getAccessibilityAccessUrl(),
                reportLabels.getAccessibilityDownloadUrl(),
                reportLabels.getDcatApCompliance(),
                reportLabels.getMachineReadability(),
                reportLabels.getCatalogueRating()
        };

        JsonObject scoreTranslations = new JsonObject()
                .put("good", reportLabels.getScoreGood())
                .put("excellent", reportLabels.getScoreExcellent())
                .put("bad", reportLabels.getScoreBad())
                .put("justEnough", reportLabels.getScoreJustEnough());
        labelCreator.getCountries().put("scoreTranslations", scoreTranslations);

        return new TableCreator(headerCells, task.getGlobalDashboard(), labelCreator.getCountries(), styles)
                .createTable();
    }


    /*
     * Method returns a Catalogue with Section objects which include chart images and their headings for a specific section (dimension)
     */
    private Future<Void> createCatalogue(MetricsPdfDocument metricsPdfDocument, JsonObject catalogueData, ReportLabelCreator labelCreator) {
        return Future.future(createCatalogue -> {
            Catalogue catalogue = new Catalogue(catalogueData);

            // Extracting Sections and saving them to the Catalogue Object
            PdfUtils.extractSectionsFromJson(catalogue, labelCreator, doughnut, bar);

            List<Future> sections = catalogue.getSections().stream()
                    .map(this::saveChartImagesToSection)
                    .collect(Collectors.toList());

            CompositeFuture.all(sections).setHandler(extractSections -> {
                if (extractSections.succeeded()) {
                    log.debug("Sections extracted from the imported JSON object for catalogue: " + catalogue.getTitle() + " " + catalogueCount++);
                    addCatalogueDataToDocument(metricsPdfDocument, catalogue);
                    createCatalogue.complete();
                } else {
                    createCatalogue.fail(extractSections.cause());
                }
            });
        });
    }

    /*
     * Method saves Image objects to the ChartBundle object within the ChartBundleArray of a Section
     */
    private Future<Void> saveChartImagesToSection(Section section) {
        return Future.future(saveChart -> {
            ArrayList<Future> futureImageList = new ArrayList<>();

            for (ChartBundle chartBundle : section.getChartBundles()) {
                JsonObject chartTemplate = chartBundle.getChartTemplate();

                Promise<Image> imagePromise = Promise.promise();
                futureImageList.add(imagePromise.future());

                if (chartTemplate == null) {
                    imagePromise.complete();
                } else {
                    client.postAbs(config().getString(ENV_QUICKCHART_ADDRESS, DEFAULT_QUICKCHART_ADDRESS))
                            .sendJsonObject(chartTemplate, ar -> {
                                if (ar.succeeded()) {
                                    // Obtain response from Quickchart API
                                    HttpResponse<Buffer> response = ar.result();
                                    ImageData imgData = ImageDataFactory.create(response.body().getBytes());
                                    Image image = new Image(imgData);
                                    //Image image = new Image(ImageDataFactory.create(response.body().getBytes()));
                                    image.scaleAbsolute(300f, 200f);

                                    chartBundle.setImage(image);
                                    imagePromise.complete(image);
                                } else {
                                    imagePromise.fail(ar.cause());
                                }
                            });
                }
            }

            CompositeFuture.all(futureImageList).setHandler(ar -> {
                if (ar.succeeded()) {
                    saveChart.complete();
                } else {
                    saveChart.fail(ar.cause());
                }
            });
        });
    }


    /*
     * Method creates a horizontal blue stroke on the page
     */
    private void setBlueStroke(MetricsPdfDocument metricsPdfDocument, boolean isCataloguePage) {
        Color blueColor = new DeviceRgb(114, 214, 234);

        PdfCanvas canvas = new PdfCanvas(metricsPdfDocument.getPdfDoc().getPage(metricsPdfDocument.getPdfDoc().getNumberOfPages()));
        canvas.setLineWidth(1.5f).setStrokeColor(blueColor);

        Rectangle mediabox = metricsPdfDocument.getPdfDoc().getPage(metricsPdfDocument.getPdfDoc().getNumberOfPages()).getMediaBox();

        float width = mediabox.getWidth();
        float height = mediabox.getHeight();


        if (isCataloguePage) {
            canvas.moveTo(mediabox.getX() + 35, height - 98).lineTo(width - 40, height - 98);
        } else {
            canvas.moveTo(mediabox.getX() + 35, height - 70).lineTo(width - 40, height - 70);
        }

        canvas.stroke();
    }


    // handles displaying of page numbers
    protected class PageNumber implements IEventHandler {

        protected MetricsPdfDocument metricsPdfDocument;
        protected ReportLabels reportLabels;
        protected PdfFormXObject placeholder;
        protected float side = 20;
        protected float xUrl = 35;
        protected float xPageNumber = 550;
        protected float y = 25;

        public PageNumber(MetricsPdfDocument metricsPdfDocument, ReportLabels reportLabels) {
            this.metricsPdfDocument = metricsPdfDocument;
            this.reportLabels = reportLabels;
            this.placeholder = new PdfFormXObject(new Rectangle(0, 0, side, side));
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdfDoc = docEvent.getDocument();
            PdfPage page = docEvent.getPage();
            int pageNumber = pdfDoc.getPageNumber(page);
            Rectangle pageSize = page.getPageSize();
            PdfCanvas pdfCanvas = new PdfCanvas(
                    page.newContentStreamBefore(), page.getResources(), pdfDoc);
            Canvas canvas = new Canvas(pdfCanvas, pdfDoc, pageSize);

            //Add header and footer
            if (pdfDoc.getPageNumber(page) > 1) {
                Paragraph dataPortal = new Paragraph().addStyle(metricsPdfDocument.getStyles().getFooterStyle())
                        .add(reportLabels.getDashboardTitle());
                canvas.showTextAligned(dataPortal, xUrl, y, TextAlignment.LEFT);
                Paragraph pageNumberParagraph = new Paragraph().addStyle(metricsPdfDocument.getStyles().getFooterStyle())
                        .add(reportLabels.getPageTranslation() + " ").add(String.valueOf(pageNumber));
                canvas.showTextAligned(pageNumberParagraph, xPageNumber, y, TextAlignment.RIGHT);
            }

            pdfCanvas.release();
        }
    }

    private Future<Buffer> readFileFromResources(String fileName) {
        return Future.future(readFile ->
                vertx.fileSystem().readFile(fileName, readFile));
    }
}
