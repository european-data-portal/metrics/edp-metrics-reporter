package io.piveau.metrics.reporter;

import io.piveau.dcat.DCATAPGraph;
import io.piveau.dcat.Dataset;
import io.piveau.dcat.Distribution;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.multipart.MultipartForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static io.piveau.metrics.reporter.ApplicationConfig.*;


public class UploadVerticle extends AbstractVerticle {

    public static final String ADDRESS = "upload-message-receiver";
    private static final List<String> FORMATS = List.of("pdf", "ods", "xlsx");
    private static final Logger log = LoggerFactory.getLogger(UploadVerticle.class);

    private WebClient client;

    @Override
    public void start(Promise<Void> startPromise) {
        client = WebClient.create(vertx);

        vertx.eventBus().consumer(ADDRESS, message -> {
            String reportDir = (String) message.body();

            uploadDatasetToHub(reportDir).setHandler(uploadReports -> {
                if (uploadReports.succeeded()) {
                    log.info("Successfully uploaded reports");
                } else {
                    log.error("Creating reports failed", uploadReports.cause());
                }
            });
        });

        startPromise.complete();
    }

    private Future<String> uploadDatasetToHub(String reportDir) {
        return Future.future(uploadDataset ->
                initHubDataset().setHandler(datasetPresent -> {
                    if (datasetPresent.succeeded()) {
                        handleDatasetToPiveau(reportDir).setHandler(res -> {
                            if (res.succeeded()) {
                                uploadDataset.complete();
                            } else {
                                uploadDataset.fail(res.cause());
                            }
                        });
                    } else {
                        uploadDataset.fail(datasetPresent.cause());
                    }
                }));
    }

    private Future<Void> handleDatasetToPiveau(String reportDir) {
        return Future.future(datasetToPiveau -> {
            DCATAPGraph graph = new DCATAPGraph().addDataset(getDataset());
            Buffer bufferGraph = Buffer.buffer();
            bufferGraph.appendString(graph.asTurtle());

            client.putAbs(config().getString(ENV_HUB_ADDRESS, DEFAULT_HUB_ADDRESS))
                    .putHeader("Authorization", config().getString(ENV_API_KEY_HUB, API_KEY_HUB))
                    .putHeader("Content-Type", "text/turtle")
                    .sendBuffer(bufferGraph, ar -> {
                        if (ar.succeeded()) {

                            for (Object it : ar.result().bodyAsJsonObject().getJsonArray("distributions")) {
                                JsonObject iterator = (JsonObject) it;
                                String ident = iterator.getString("identifier");
                                String uploadUrl = iterator.getString("upload_url");

                                vertx.fileSystem().readDir(reportDir, readDir -> {
                                    if (readDir.succeeded()) {


                                        List<Future> reportPostList = new ArrayList<>();

                                        readDir.result().forEach(fileName -> {
                                            String[] mustContain = ident.split("-");

                                            if (mustContain.length == 2) {
                                                if (fileName.contains(mustContain[0]) && fileName.contains(mustContain[1])) {
                                                    log.info("add: " + fileName);
                                                    reportPostList.add(postReportDataUploader(uploadUrl, fileName));
                                                }
                                            }
                                        });

                                        CompositeFuture.all(reportPostList).setHandler(res -> {
                                            if (res.succeeded()) {
                                                log.debug("Posting all reports to data upload completed");
                                                if (!datasetToPiveau.future().isComplete()) datasetToPiveau.complete();
                                            } else {
                                                datasetToPiveau.fail(res.cause());
                                            }
                                        });


                                    } else {
                                        datasetToPiveau.fail(readDir.cause());
                                    }
                                });
                            }
                        } else {
                            datasetToPiveau.fail(ar.cause());
                        }
                    });
        });
    }

    private Future<Void> postReportDataUploader(String url, String fileName) {
        return Future.future(postReport -> {
            String mimeType = "";

            if (fileName.contains(".pdf")) mimeType = "application/pdf";
            else if (fileName.contains(".xls")) mimeType = "application/msexcel";
            else if (fileName.contains(".ods")) mimeType = "application/octet-stream";

            MultipartForm fileUpload = MultipartForm.create()
                    .binaryFileUpload("file", fileName, fileName, mimeType);

            log.info("upload file to url: " + url);

            client.postAbs(url)
                    .putHeader("Authorization", config().getString(ENV_API_KEY_UPLOAD, API_KEY_UPLOAD))
                    .sendMultipartForm(fileUpload, ar -> {
                        if (ar.succeeded()) {
                            postReport.complete();
                        } else {
                            postReport.fail(ar.cause());
                        }
                    });
        });
    }

    private Future<Void> initHubDataset() {
        return Future.future(checkDatasetPresence ->
                client.getAbs(config().getString(ENV_HUB_ADDRESS, DEFAULT_HUB_ADDRESS))
                        .putHeader("Authorization", config().getString(ENV_API_KEY_HUB, API_KEY_HUB))
                        .putHeader("Accept", "*/*")
                        .send(ar -> {
                            if (ar.succeeded()) {
                                if (ar.result().statusCode() == 404 || ar.result().statusCode() == 200) {
                                    client.deleteAbs(config().getString(ENV_HUB_ADDRESS, DEFAULT_HUB_ADDRESS))
                                            .putHeader("Authorization", config().getString(ENV_API_KEY_HUB, API_KEY_HUB))
                                            .send(res -> {
                                                if (res.succeeded()) {
                                                    checkDatasetPresence.complete();
                                                } else {
                                                    checkDatasetPresence.fail(res.cause());
                                                }
                                            });
                                }
                            } else {
                                checkDatasetPresence.fail(ar.cause());
                            }
                        }));
    }

    private Dataset getDataset() {
        String datasetURI = "http://www.example.com/set/data/123";
        Dataset dataset = new Dataset(datasetURI);

        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        String description = "The Metadata Quality Assessment (MQA) is a tool developed by the consortium of the European Data Portal to study the quality of metadata harvested by the European Data Portal. It is intended to help data providers and data portals to check their metadata quality and to receive suggestions for improvements. The results are presented via the MQA and are also available as download. In the following we describe the functionality of the MQA and the methodology it uses.\n "
                + "This record contains the MQA report for the " + date + "\n"
                + "The report is available in the following formats: .pdf, .ods, .xlsx \n"
                + "More information about the way the MQA works can be found here: https://www.europeandataportal.eu/mqa/methodology";
        dataset
                .addTitle("MQA Reports " + date, "de")
                .addDescription(description)
                .addIssued(Instant.now())
                .addModified(Instant.now())
                .addPublisher("European Data Portal", "https://www.europeandataportal.eu")
                .addContactPoint("EDP Service 1", "help@europeandataportal.eu");

        generateDistributions().forEach(dataset::addDistribution);

        return dataset;
    }

    private List<Distribution> generateDistributions() {
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        String description = "This record contains the MQA report for the " + date + "\n";

        List<Distribution> distributions = new ArrayList<>();

        config().getJsonArray(ENV_LANGUAGES, DEFAULT_LANGUAGES).forEach(lang ->
                FORMATS.forEach(format ->
                        distributions.add(new Distribution()
                                .addFormat(format)
                                .addTitle("Report-" + lang + "-" + format)
                                .addIdentifier(lang + "-" + format)
                                .addDescription(description)
                                .addLicense("http://publications.europa.eu/resource/authority/licence/CC_BYSA"))));

        return distributions;
    }
}