package io.piveau.metrics.reporter;

import io.piveau.metrics.reporter.model.Catalogue;
import io.piveau.metrics.reporter.model.ReportLabels;
import io.piveau.metrics.reporter.model.Section;
import io.piveau.metrics.reporter.utils.ReportLabelCreator;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class XlsReport {

    private List<Catalogue> catalogues;
    private JsonArray overviewTableData;
    private ReportLabelCreator labelCreator;
    private String fileName;

    private static final Logger log = LoggerFactory.getLogger(XlsReport.class);

    public XlsReport(List<Catalogue> catalogues, JsonArray overviewTableData, ReportLabelCreator labelCreator, String fileName) {
        this.catalogues = catalogues;
        this.overviewTableData = overviewTableData;
        this.labelCreator = labelCreator;
        this.fileName = fileName;
    }

    public void createXlsReport() {

        try (XSSFWorkbook workbook = new XSSFWorkbook(); OutputStream outputStream = new FileOutputStream(fileName)) {

            ReportLabels reportLabels = labelCreator.getReportLabels();

            // creating a font of height 12pt and bold font weight with blue colored background
            XSSFCellStyle styleBoldFontBlueBackground = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setFontHeightInPoints((short) 12);
            font.setBold(true);
            font.setItalic(false);
            styleBoldFontBlueBackground.setFont(font);
            styleBoldFontBlueBackground.setFillForegroundColor(new XSSFColor(new Color(159, 242, 223), new DefaultIndexedColorMap()));
            styleBoldFontBlueBackground.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // creating a bold font
            XSSFCellStyle styleBoldFont = workbook.createCellStyle();
            styleBoldFont.setFont(font);

            // inserting overview page
            XSSFSheet overviewSheet = workbook.createSheet(reportLabels.getNavigationDashboard());
            XSSFRow overviewMainHeadingRow = overviewSheet.createRow(0);
            overviewMainHeadingRow.createCell(0).setCellValue(reportLabels.getNavigationDashboard());
            overviewMainHeadingRow.getCell(0).setCellStyle(styleBoldFont);

            String[] headerCells = {reportLabels.getCatalogueCountry(),
                    reportLabels.getCatalogueName(),
                    reportLabels.getAccessibilityAccessUrl(),
                    reportLabels.getAccessibilityDownloadUrl(),
                    reportLabels.getDcatApCompliance(),
                    reportLabels.getMachineReadability(),
                    reportLabels.getCatalogueRating()
            };

            int overviewRowCount = 2;

            // inserting header cells for overview page
            XSSFRow overviewHeaderCellsRow = overviewSheet.createRow(overviewRowCount++);
            overviewHeaderCellsRow.setRowStyle(styleBoldFontBlueBackground);
            int columnIndex = 0;
            for (String cellName : headerCells) {
                overviewHeaderCellsRow.createCell(columnIndex).setCellValue(cellName);
                overviewHeaderCellsRow.getCell(columnIndex).setCellStyle(styleBoldFontBlueBackground);
                columnIndex++;
            }

            // iterating over data for overview table and adding it to the sheet
            for (Object arrayItem : overviewTableData) {
                int cellCount = 0;
                JsonObject obj = (JsonObject) arrayItem;
                JsonObject info = obj.getJsonObject("info");

                XSSFRow row = overviewSheet.createRow(overviewRowCount++);
                JsonObject countries = labelCreator.getSectionTranslations().getJsonObject("countries");

                // catalogue country
                if (info.getString("spatial") == null || !countries.containsKey(info.getString("spatial"))) {
                    row.createCell(cellCount++).setCellValue("-");
                } else {
                    row.createCell(cellCount++).setCellValue(countries.getString(info.getString("spatial")));
                }

                // catalogue title
                row.createCell(cellCount++).setCellValue(getStringValue(info, "title"));

                JsonObject accessibility = obj.getJsonObject("accessibility");
                // accessibility access URL
                row.createCell(cellCount++).setCellValue(getArrayStringValue(accessibility, "accessUrlStatusCodes"));
                // accessibility download URL
                row.createCell(cellCount++).setCellValue(getArrayStringValue(accessibility, "downloadUrlStatusCodes"));

                JsonObject interoperability = obj.getJsonObject("interoperability");
                // DCAT-AP compliance
                row.createCell(cellCount++).setCellValue(getArrayStringValue(interoperability, "dcatApCompliance"));
                // machine readability
                row.createCell(cellCount++).setCellValue(getArrayStringValue(interoperability, "formatMediaTypeMachineReadable"));

                row.createCell(cellCount).setCellValue(getScoreValue(obj));
            }

            // inserting sheets for catalogues
            for (Catalogue catalogue : catalogues) {
                // Creating Sheet with Catalogue title as name
                XSSFSheet spreadSheet = workbook.createSheet(catalogue.getTitle() + " - " + catalogue.getId());

                // setting catalogue title as title in the first row of the sheet
                XSSFRow catalogueHeaderRow = spreadSheet.createRow(0);
                catalogueHeaderRow.createCell(0).setCellValue(catalogue.getTitle());
                catalogueHeaderRow.getCell(0).setCellStyle(styleBoldFont);

                int rowCount = 2;
                for (Section section : catalogue.getSections()) {
                    // add some empty rows
                    spreadSheet.createRow(rowCount++);
                    spreadSheet.createRow(rowCount++);
                    // setting section heading
                    XSSFRow sectionHeaderRow = spreadSheet.createRow(rowCount++);
                    sectionHeaderRow.setRowStyle(styleBoldFontBlueBackground);
                    sectionHeaderRow.createCell(0).setCellValue(section.getSectionHeading());
                    sectionHeaderRow.getCell(0).setCellStyle(styleBoldFontBlueBackground);

                    for (Map.Entry<String, Object> sectionEntry : section.getSectionData()) {
                        // setting heading of the indicator
                        XSSFRow indicatorHeaderRow = spreadSheet.createRow(rowCount++);
                        indicatorHeaderRow.createCell(0).setCellValue(labelCreator.getSectionTranslations().getString(sectionEntry.getKey()));
                        indicatorHeaderRow.getCell(0).setCellStyle(styleBoldFont);

                        JsonArray indicatorData = (JsonArray) sectionEntry.getValue();

                        if (indicatorData != null && !indicatorData.isEmpty()) {

                            ArrayList<String> keys = new ArrayList<>();
                            ArrayList<Double> values = new ArrayList<>();
                            for (int i = 0; i < indicatorData.size(); i++) {
                                JsonObject indicator = indicatorData.getJsonObject(i);
                                if (indicator.getString("name").equals("yes")) {
                                    keys.add(reportLabels.getMetricLabelYes());
                                } else if (indicator.getString("name").equals("no")) {
                                    keys.add(reportLabels.getMetricLabelNo());
                                } else {
                                    keys.add(indicator.getString("name"));
                                }
                                values.add(indicator.getDouble("percentage"));
                            }
                            // save keys and values to two ArrayLists, print keys in one row and values in a row below it
                            XSSFRow indicatorKeysRow = spreadSheet.createRow(rowCount++);
                            int keyCells = 0;
                            for (String key : keys) {
                                indicatorKeysRow.createCell(keyCells++).setCellValue(key);
                            }
                            XSSFRow indicatorValuesRow = spreadSheet.createRow(rowCount++);
                            int valueCells = 0;
                            for (Double value : values) {
                                indicatorValuesRow.createCell(valueCells++).setCellValue(value);
                            }
                        } else {
                            XSSFRow indicatorNotFoundRow = spreadSheet.createRow(rowCount++);
                            indicatorNotFoundRow.createCell(0).setCellValue("n/a");
                        }
                        spreadSheet.createRow(rowCount++);
                    }
                }
            }

            workbook.write(outputStream);
        } catch (IOException e) {
            log.error("Failed to create XLS");
        }
    }

    private String getStringValue(JsonObject motherObject, String key) {
        if (motherObject.getString(key) == null) {
            return "n/a";
        } else {
            return motherObject.getString(key);
        }
    }

    private String getArrayStringValue(JsonObject motherObject, String key) {
        JsonArray keyArray = motherObject.getJsonArray(key);
        if (keyArray == null || keyArray.isEmpty()) {
            return "n/a";
        } else {
            String returnString = "-";
            for (Object item : keyArray) {
                JsonObject itemObject = (JsonObject) item;
                if (itemObject.getString("name").equals("yes") || itemObject.getString("name").equals("200")) {
                    returnString = itemObject.getDouble("percentage").toString();
                }
            }
            return returnString;
        }
    }

    private String getScoreValue(JsonObject motherObject) {
        if (motherObject.getInteger("score") == null) {
            return "-";
        } else {
//            Excellent: 351 - 405
//            Good: 221 - 350
//            Just enough: 121 - 220
//            Bad: 0 - 120
            int score = motherObject.getInteger("score");
            if (score < 121) {
                return labelCreator.getReportLabels().getScoreBad();
            } else if (score < 221) {
                return labelCreator.getReportLabels().getScoreJustEnough();
            } else if (score < 351) {
                return labelCreator.getReportLabels().getScoreGood();
            } else if (score < 405) {
                return labelCreator.getReportLabels().getScoreExcellent();
            } else {
                return "n/a";
            }
        }
    }
}