package io.piveau.metrics.reporter;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.Assert.assertEquals;


@DisplayName("Testing the Metrics Verticle")
@ExtendWith(VertxExtension.class)
class MetricsVerticleTest {
//
//    private Integer port;
//
//    @BeforeEach
//    void startVerticle(Vertx vertx, VertxTestContext testContext) {
//
//
//        port = 9090;
//
//        JsonObject metricsConf = new JsonObject()
//                .put(ApplicationConfig.ENV_METRICS_HOST, ApplicationConfig.DEFAULT_METRICS_HOST)
//                .put(ApplicationConfig.ENV_METRICS_PORT, 80);
//
//
//        vertx.deployVerticle(new MetricsVerticle(), new DeploymentOptions().setConfig(metricsConf),
//                testContext.succeeding(response -> testContext.verify(testContext::completeNow)));
//    }
//
//    @After
//    public void tearDown(Vertx vertx, VertxTestContext testContext) {
//        vertx.close();
//    }
//
//
////    Test creates server and serves a JSON object as string if /api is called. It then triggers
////     the Metrics Verticle which returns the string from /api as a JSON Object, eventually
////     it is checked that the two JSON Objects are equal.
//
//
//    @Test
//    @DisplayName("Receive JsonObject from Metrics")
//    void receiveDataFromMetricsAsJsonObject(Vertx vertx, VertxTestContext testContext) {
//
//        HttpServer server = vertx.createHttpServer();
//        String metricsUri = "/api";
//        JsonObject message = new JsonObject()
//                .put("type", "JsonObject")
//                .put("uri", metricsUri);
//
//
//        JsonObject jsonObject = new JsonObject().put("success", "success").put("result",
//                new JsonObject().put("no", 71.2).put("yes", 100 - 71.2));
//        server.requestHandler(req -> {
//
//            if (!req.path().equals(metricsUri)) {
//                throw new AssertionError("Wrong path!");
//            }
//
//            req.response().end(jsonObject.encodePrettily());
//        });
//
//
//        server.listen(port);
//
//
//        EventBus eventBus = vertx.eventBus();
//        eventBus.request(AddressBook.METRICS_EVENTBUS_ADDRESS, message, reply -> {
//            if (reply.succeeded()) {
//                JsonObject responseAsJsonObject = (JsonObject) reply.result().body();
//                assertEquals(jsonObject, responseAsJsonObject);
//                testContext.completeNow();
//            } else {
//                testContext.failNow(reply.cause());
//            }
//        });
//
//    }
//
//
//    @Test
//    @DisplayName("Receive JsonArray from Metrics")
//    void receiveDataFromMetricsAsJsonArray(Vertx vertx, VertxTestContext testContext) {
//
//        HttpServer server = vertx.createHttpServer();
//        String metricsUri = "/api";
//        JsonObject message = new JsonObject()
//                .put("type", "JsonArray")
//                .put("uri", metricsUri);
//
//
//        JsonArray jsonArray = new JsonArray().add("string1").add("string2");
//        server.requestHandler(req -> {
//
//            if (!req.path().equals(metricsUri)) {
//                throw new AssertionError("Wrong path!");
//            }
//
//            req.response().end(jsonArray.encodePrettily());
//        });
//
//
//        server.listen(port);
//
//        EventBus eventBus = vertx.eventBus();
//        eventBus.request(AddressBook.METRICS_EVENTBUS_ADDRESS, message, reply -> {
//            if (reply.succeeded()) {
//                JsonArray responseAsJsonArray = (JsonArray) reply.result().body();
//                assertEquals(jsonArray, responseAsJsonArray);
//                testContext.completeNow();
//            } else {
//                testContext.failNow(reply.cause());
//            }
//        });
//
//    }

}


