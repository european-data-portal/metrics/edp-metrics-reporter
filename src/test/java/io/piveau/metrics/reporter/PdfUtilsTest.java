package io.piveau.metrics.reporter;

import io.vertx.junit5.VertxExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.Assert.assertEquals;



@DisplayName("Testing PDF Utils")
@ExtendWith(VertxExtension.class)
class PdfUtilsTest {



    // Test checks that the Paragraph object returned by the generateParagraph method contains
    // the string that was handed to it.


//    @Test
//    public void testGenerateParagraph(){
//        Styles styles = new Styles();
//        String s = "Test string";
//        Paragraph paragraph = PdfUtils.generateParagraph(s, styles.getHeadingOneStyle());
//        List<IElement> elementList = paragraph.getChildren();
//
//        for (IElement iElement : elementList) {
//            Text t = (Text) iElement;
//            assertEquals(s, t.getText());
//        }
//    }


//    @Test
//    @DisplayName("Saving Section Heading and Image to Cell object")
//    void testSaveChartBundleHeadingAndImageToCell(Vertx vertx) {
//        JsonArray data = new JsonArray();
//        JsonObject template = new JsonObject();
//        ChartBundle testBundle = new BarChartBundle("testChart", data, template, false, "");
//        Image testChartImage = null;
//        try {
//            testChartImage = new Image(ImageDataFactory.create("mqa_report_header.png"));
//        } catch (MalformedURLException e) {
//
//        }
//
//        testBundle.setImage(testChartImage);
//
//        Styles styles = new Styles();
//        Cell testCell = PdfUtils.saveChartBundleHeadingAndImageToCell(testBundle, styles);
//
//        List<IElement> cellElements = testCell.getChildren();
//        for (IElement cellElement : cellElements){
//            Table table = (Table) cellElement;
//            List<IElement> tableElements = table.getChildren();
//            for (IElement tableElement : tableElements){
//                Cell nestedCell = (Cell) tableElement;
//                List<IElement> nestedCellElements = nestedCell.getChildren();
//                for (IElement nestedCellElement : nestedCellElements){
//                    if (nestedCellElement.getClass().getName().equals("com.itextpdf.layout.element.Paragraph")){
//                        Paragraph heading = (Paragraph) nestedCellElement;
//                        List<IElement> headingElements = heading.getChildren();
//                        for (IElement headingElement : headingElements){
//                            Text headingText = (Text) headingElement;
//                            assertEquals(headingText.getText(), testBundle.getChartName());
//                        }
//                    } else if (nestedCellElement.getClass().getName().equals("com.itextpdf.layout.element.Image")){
//                        assertEquals((Image) nestedCellElement, testChartImage);
//                    }
//                }
//            }
//        }
//
//
//    }
}

