package io.piveau.metrics.reporter;

import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


@DisplayName("Testing generate and upload reports Utils")
@ExtendWith(VertxExtension.class)
public class UploadReportsFullTest {

//    private static final Logger LOG = LoggerFactory.getLogger(VertxExtension.class);
//
//    @BeforeEach
//    void startVerticle(Vertx vertx, VertxTestContext testContext) {
//
//        JsonObject pdfConf = new JsonObject()
//                .put(ApplicationConfig.QUICKCHART_HOST, ApplicationConfig.DEFAULT_QUICKCHART_HOST)
//                .put(ApplicationConfig.QUICKCHART_PORT, ApplicationConfig.DEFAULT_QUICKCHART_PORT)
//                .put(ApplicationConfig.QUICKCHART_URI, ApplicationConfig.DEFAULT_QUICKCHART_URI);
//
//
//        Promise<String> pdfVerticleDeployment = Promise.promise();
//        vertx.deployVerticle(new PdfCreatorVerticle(), new DeploymentOptions().setConfig(pdfConf),
//                pdfVerticleDeployment);
//
//        // Creating configuration for MetricsVerticle
//        JsonObject metricsConf = new JsonObject()
//                .put(ApplicationConfig.METRICS_HOST, ApplicationConfig.DEFAULT_METRICS_HOST)
//                .put(ApplicationConfig.METRICS_PORT, ApplicationConfig.DEFAULT_METRICS_PORT);
//
//        Promise<String> metricsVerticleDeployment = Promise.promise();
//        vertx.deployVerticle(new MetricsVerticle(), new DeploymentOptions().setConfig(metricsConf),
//                metricsVerticleDeployment);
//
//
//        // Upload Verticle
//        JsonObject uploadConf = new JsonObject();
//        Promise<String> uploadVerticleDeployment = Promise.promise();
//        vertx.deployVerticle(new UploadVerticle(), new DeploymentOptions().setConfig(uploadConf),
//                uploadVerticleDeployment);
//
//
//        // Creating configuration for XlsVerticle
//        JsonObject xlsConf = new JsonObject();
//        Promise<String> xlsVerticleDeployment = Promise.promise();
//        vertx.deployVerticle(new XlsCreatorVerticle(), new DeploymentOptions().setConfig(xlsConf),
//                xlsVerticleDeployment);
//
//
//        // Creating configuration for OdsVerticle
//        JsonObject odsConf = new JsonObject();
//        Promise<String> odsVerticleDeployment = Promise.promise();
//        vertx.deployVerticle(new OdsCreatorVerticle(), new DeploymentOptions().setConfig(odsConf),
//                odsVerticleDeployment);
//
//        CompositeFuture.all(pdfVerticleDeployment.future(), uploadVerticleDeployment.future(), metricsVerticleDeployment.future(), xlsVerticleDeployment.future(), odsVerticleDeployment.future()).setHandler(h -> {
//            if (h.succeeded()) {
//                testContext.completeNow();
//            } else {
//                testContext.failed();
//            }
//        });
//    }
//
//
//    @After
//    public void tearDown(Vertx vertx, VertxTestContext testContext) {
//        vertx.close();
//    }
//
//    /**
//     *
//     * In this method, run the reports creator method generating all reports in every language possible
//     * @param vertx
//     */
//    @Test
//    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
//    public void getReportsTest(Vertx vertx, VertxTestContext testContext) {
//        EventBus eventBus = vertx.eventBus();
//        WebClient client = WebClient.create(vertx);
//
//        client.deleteAbs("https://www.ppe-aws.europeandataportal.eu/data/api/datasets/test-dataset?catalogue=edp&data=true")
//                .putHeader("Authorization", "c419ee76-7002-11e9-a923-1681be663d3e")
//                .send(ar -> {
//                    if (ar.succeeded()) {
//                        LOG.info("Deleting example dataset. Create new one");
//
//                        DeliveryOptions options = new DeliveryOptions().setSendTimeout(1000000);
//
//                        eventBus.request(AddressBook.UPLOAD_EVENTBUS_ADDRESS, null, options, res -> {
//                            LOG.info("Send message via event bus");
//                            if (res.succeeded()) {
//                                // success
//                                testContext.completeNow();
//                            } else {
//                                testContext.failNow(res.cause());
//                            }
//                        });
//
//                    }
//                });
//    }

}

