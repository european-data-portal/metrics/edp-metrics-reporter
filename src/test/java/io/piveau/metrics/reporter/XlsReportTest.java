package io.piveau.metrics.reporter;

import io.vertx.junit5.VertxExtension;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;



@DisplayName("Testing XLS Report")
@ExtendWith(VertxExtension.class)
class XlsReportTest {



    // Test checks that the byte[] returned by the createXlsByteArray method is not null.

    /*

//    @Test
    public void testXlsByteArrayCreation(Vertx vertx){

        FileSystem vertxFilesystem = vertx.fileSystem();

        JsonArray overviewTableData = new JsonArray("[ {\n" +
                "  \"score\" : null,\n" +
                "  \"accessibility\" : {\n" +
                "    \"accessUrlStatusCodes\" : [ ],\n" +
                "    \"downloadUrlStatusCodes\" : [ ]\n" +
                "  },\n" +
                "  \"interoperability\" : {\n" +
                "    \"formatMediaTypeMachineReadable\" : null,\n" +
                "    \"dcatApCompliance\" : null\n" +
                "  },\n" +
                "  \"info\" : {\n" +
                "    \"id\" : \"avaandmete-portaal\",\n" +
                "    \"title\" : \"Avaandmete portaal\",\n" +
                "    \"description\" : \"Open Data Portal Estonia\",\n" +
                "    \"spatial\" : \"EST\",\n" +
                "    \"type\" : \"dcat-ap\"\n" +
                "  }\n" +
                "}]");


        JsonObject testObject = new JsonObject(vertx.fileSystem().readFileBlocking("ExampleMetricsJson.json"));

        Locale locale = new Locale("en");
        ReportLabelCreator reportLabelCreator = new ReportLabelCreator(locale);

        List<Catalogue> catalogueDataArray = new ArrayList<>();
        Catalogue testCatalogue = new Catalogue("ID","Catalogue Heading", "Catalogue Description", "Catalogue Spatial", "Type");
//        testCatalogue.extractSectionsFromJson(testObject, vertx.fileSystem(), "Error");
        catalogueDataArray.add(testCatalogue);

        ReportLabels reportLabels = new ReportLabels();
        reportLabels.setCatalogueCountry("en");
        reportLabels.setCatalogueName("Name");
        reportLabels.setCatalogueRating("Score");
        reportLabels.setAccessibilityAccessUrl("Access URL");
        reportLabels.setAccessibilityDownloadUrl("Download URL");
        reportLabels.setMachineReadability("Machine Readable");
        reportLabels.setDcatApCompliance("DCAT-AP Compliance");
        reportLabels.setNavigationDashboard("Overview");

        JsonObject translations = reportLabelCreator.getSectionTranslations(vertxFilesystem)
            .put("countries", new JsonObject(vertxFilesystem.readFileBlocking("i18n/countries.json")));

        XlsReport xlsReport = new XlsReport(catalogueDataArray, overviewTableData, reportLabels, translations);
        xlsReport.createXlsReport();

        try (FileInputStream xlsxInput = new FileInputStream("thefilename" + ".xlsx")) {
            XSSFWorkbook xlsWorkbook = new XSSFWorkbook(xlsxInput);
            assertEquals(2, xlsWorkbook.getNumberOfSheets());
            assertEquals("Catalogue Heading - ID", xlsWorkbook.getSheetName(1));
            assertEquals("Contextuality", xlsWorkbook.getSheetAt(1).getRow(4).getCell(0).getStringCellValue());
        } catch (IOException e) {
            fail("Could not create XLS Workbook from provided file: " + e);
        }
    }

     */
}

