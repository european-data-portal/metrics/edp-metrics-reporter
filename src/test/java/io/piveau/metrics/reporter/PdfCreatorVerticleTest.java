package io.piveau.metrics.reporter;

import io.vertx.core.Vertx;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.After;

/*

@DisplayName("Testing the Pdf Verticle")
@ExtendWith(VertxExtension.class)
class PdfCreatorVerticleTest {

    @BeforeEach
    void startVerticle(Vertx vertx, VertxTestContext testContext) {

        JsonObject pdfConf = new JsonObject()
            .put(ApplicationConfig.QUICKCHART_HOST, ApplicationConfig.DEFAULT_QUICKCHART_HOST)
            .put(ApplicationConfig.QUICKCHART_PORT, ApplicationConfig.DEFAULT_QUICKCHART_PORT)
            .put(ApplicationConfig.QUICKCHART_URI, ApplicationConfig.DEFAULT_QUICKCHART_URI);

        vertx.deployVerticle(new PdfCreatorVerticle(), new DeploymentOptions().setWorker(true).setConfig(pdfConf),
                testContext.succeeding(response -> testContext.verify(testContext::completeNow)));
    }


    @After
    public void tearDown(Vertx vertx, VertxTestContext testContext) {
        vertx.close();
    }


    

}

 */